# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170919131532) do

  create_table "Projects_Users", id: false, force: :cascade do |t|
    t.integer "Project_id", null: false
    t.integer "User_id", null: false
    t.string "level", limit: 1, null: false
  end

  create_table "microposts", force: :cascade do |t|
    t.integer "user_id"
    t.string "commit"
    t.string "tex"
    t.string "html"
    t.string "content_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_id"
    t.index ["project_id"], name: "index_microposts_on_project_id"
    t.index ["user_id"], name: "index_microposts_on_user_id"
  end

  create_table "microposts_reports", id: false, force: :cascade do |t|
    t.integer "micropost_id"
    t.integer "report_id"
    t.index ["micropost_id"], name: "index_microposts_reports_on_micropost_id"
    t.index ["report_id"], name: "index_microposts_reports_on_report_id"
  end

  create_table "projects", force: :cascade do |t|
    t.integer "owner_id"
    t.string "name"
    t.string "share_link"
    t.boolean "is_shared_with_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.string "git_url"
    t.index ["owner_id"], name: "index_projects_on_owner_id"
  end

  create_table "publications", force: :cascade do |t|
    t.string "ref"
    t.string "title"
    t.string "author"
    t.string "bibtex"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pdf_file_name"
    t.string "pdf_content_type"
    t.integer "pdf_file_size"
    t.datetime "pdf_updated_at"
    t.integer "user_id"
    t.integer "project_id"
    t.index ["project_id"], name: "index_publications_on_project_id"
    t.index ["user_id"], name: "index_publications_on_user_id"
  end

  create_table "reports", force: :cascade do |t|
    t.integer "owner_id"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "share_link"
    t.boolean "is_shared_with_link"
    t.index ["owner_id"], name: "index_reports_on_owner_id"
  end

  create_table "user_projects", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.string "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_user_projects_on_project_id"
    t.index ["user_id"], name: "index_user_projects_on_user_id"
  end

  create_table "user_reports", force: :cascade do |t|
    t.integer "user_id"
    t.integer "report_id"
    t.string "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_user_reports_on_report_id"
    t.index ["user_id"], name: "index_user_reports_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30, null: false
    t.string "username"
    t.string "username_lowercase"
    t.integer "current_project_id"
    t.integer "current_report_id"
    t.index ["current_project_id"], name: "index_users_on_current_project_id"
    t.index ["current_report_id"], name: "index_users_on_current_report_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
