class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.belongs_to :owner, foreign_key: true, foreign_key: {to_table: :users}
      t.string :name
      t.string :share_link
      t.boolean :is_shared_with_link

      t.timestamps
    end
  end
end
