class AddCurrentProjectToUsers < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :current_project 
  end
end
