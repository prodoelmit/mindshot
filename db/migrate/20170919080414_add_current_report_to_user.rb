class AddCurrentReportToUser < ActiveRecord::Migration[5.1]
  def change
      add_reference :users, :current_report
  end
end
