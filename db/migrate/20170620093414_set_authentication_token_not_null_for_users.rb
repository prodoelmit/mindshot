class SetAuthenticationTokenNotNullForUsers < ActiveRecord::Migration[5.1]
  def change
      change_column :users, :authentication_token, :string, null: false
  end
end
