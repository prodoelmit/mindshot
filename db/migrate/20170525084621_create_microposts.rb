class CreateMicroposts < ActiveRecord::Migration[5.1]
  def change
    create_table :microposts do |t|

      t.belongs_to :user
      t.string :commit
      t.string :tex
      t.string :html
      t.string :content_path



      t.timestamps
    end
  end
end
