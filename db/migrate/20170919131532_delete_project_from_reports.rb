class DeleteProjectFromReports < ActiveRecord::Migration[5.1]
  def change
      remove_column :reports, :project_id
  end
end
