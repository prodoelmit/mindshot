class ChangeReportUserColumnName < ActiveRecord::Migration[5.1]
  def change
      rename_column :reports, :user_id, :owner_id
  end
end
