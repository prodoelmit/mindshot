class CreateMicropostReportJoinTable < ActiveRecord::Migration[5.1]
  def change
      create_table :microposts_reports, id: false do |t|
          t.belongs_to :micropost, index: true
          t.belongs_to :report, index: true
      end
  end
end
