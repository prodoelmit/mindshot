class CreateUserReports < ActiveRecord::Migration[5.1]
  def change
    create_table :user_reports do |t|
        t.references :user
        t.references :report
        t.string :level
        t.timestamps
    end
  end
end
