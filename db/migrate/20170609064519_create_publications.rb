class CreatePublications < ActiveRecord::Migration[5.1]
  def change
    create_table :publications do |t|

        t.string :ref, unique: true
        t.string :title
        t.string :author
        t.string :bibtex

      t.timestamps
    end
  end
end
