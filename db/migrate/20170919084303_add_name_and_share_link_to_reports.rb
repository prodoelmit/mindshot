class AddNameAndShareLinkToReports < ActiveRecord::Migration[5.1]
  def change
      add_column :reports, :name, :string
      add_column :reports, :share_link, :string
      add_column :reports, :is_shared_with_link, :boolean
  end
end
