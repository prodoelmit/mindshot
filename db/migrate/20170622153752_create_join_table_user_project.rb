class CreateJoinTableUserProject < ActiveRecord::Migration[5.1]
  def change
    create_join_table :Projects, :Users do |t|
        t.string :level, limit: 1, null: false
      # t.index [:user_id, :project_id]
      # t.index [:project_id, :user_id]
    end
  end
end
