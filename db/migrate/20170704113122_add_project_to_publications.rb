class AddProjectToPublications < ActiveRecord::Migration[5.1]
  def change
    add_reference :publications, :project, foreign_key: true
  end
end
