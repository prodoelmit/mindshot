class AddAttachmentPdfToPublications < ActiveRecord::Migration[4.2]
  def self.up
    change_table :publications do |t|
      t.attachment :pdf
    end
  end

  def self.down
    remove_attachment :publications, :pdf
  end
end
