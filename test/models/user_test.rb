require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should be invalid without email" do
      users(:one).email = nil
      assert_nil users(:one).email
      assert_equal false, users(:one).valid?
  end
end
