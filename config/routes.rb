Rails.application.routes.draw do
    devise_scope :user do
        authenticated :user do 
            root to: 'home#dashboard', as: :authenticated_root
        end

        unauthenticated do
            root to: 'devise/sessions#new', as: :unauthenticated_root
        end
    end

    root to: 'home#index'
  devise_for :users

  resources :microposts, except: :new, constraints: {id: /[0-9]+/}
  post '/microposts/:id/add_to_current_report', to: 'microposts#add_to_current_report', as: 'add_mp_to_current_report'
  post '/microposts/:id/remove_from_current_report', to: 'microposts#remove_from_current_report', as: 'remove_mp_from_current_report'
  resources :publications
  get '/publications/:id/pdf', to: 'publications#pdf', as: 'publication_pdf'
  get '/microposts/:id/tex', to: 'microposts#tex', as: 'micropost_tex'
  get '/publications/:id/info', to: 'publications#info', as: 'publication_info'
  post '/misc/getPublicationIds', to: 'publications#getIds'
  resources :projects do
  end
  post '/projects/:id/leave', to: 'projects#leave', as: "leave_project"
  get '/projects/:id/microposts', to: 'projects#microposts', as: "project_microposts"
  get '/projects/:id/publications', to: 'projects#publications', as: "project_publications"
  get '/projects/:id/sharesettings', to: 'projects#sharesettings', as: "project_sharesettings"
  post '/projects/:id/share_with_user', to: 'projects#share_with_user', as: "project_share_with_user"
  post '/projects/:id/set_user_permission', to: 'projects#set_user_permission', as: "project_set_user_permission"
  post '/projects/:id/kick_user', to: 'projects#kick_user', as: "project_kick_user"
  post '/projects/:id/set_as_current', to: 'projects#set_as_current', as: "project_set_as_current"
  get '/projects/:project_id/publication/:id', to: 'publications#show', as: "project_publication"
  resources :reports do 
  end
  post '/reports/:id/leave', to: 'reports#leave', as: "leave_report"
  get '/reports/:id/microposts', to: 'reports#microposts', as: "report_microposts"
  get '/reports/:id/publications', to: 'reports#publications', as: "report_publications"
  get '/reports/:id/sharesettings', to: 'reports#sharesettings', as: "report_sharesettings"
  post '/reports/:id/share_with_user', to: 'reports#share_with_user', as: "report_share_with_user"
  post '/reports/:id/set_user_permission', to: 'reports#set_user_permission', as: "report_set_user_permission"
  post '/reports/:id/kick_user', to: 'reports#kick_user', as: "report_kick_user"
  post '/reports/:id/set_as_current', to: 'reports#set_as_current', as: "report_set_as_current"
  get '/reports/:report_id/publication/:id', to: 'publications#show', as: "report_publication"

  get '/users/:username/stats', to: 'user#stats', as: "user_stats"
  get '/users/:username', to: 'user#show', as: "user"
  get '/users/:username/microposts', to: 'user#microposts', as: "user_microposts"




  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mathjax 'mathjax'

  get '/settings', to: 'user#settings'
  put '/settings/update_password', to: 'user#update_password'

end
