module Mindshot
    class Application < Rails::Application
        config.x.preamble = Pathname.new("lib/parser/preamble.tex")
        config.x.parserassetsdir = Rails.root.join("lib/parser/assets")
        config.x.formulasPath = Pathname.new("public/system/formulas")
        config.x.formulasDisplayPath = "/system/formulas"
    end


end
