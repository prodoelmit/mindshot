FactoryGirl.define do

  factory :publication do
    ref {Faker::Internet.unique.user_name(nil, %w(_ -) )}
    title {Faker::Hipster.sentence(6)}
    author {Faker::HarryPotter.character}
    bibtex { """
             @article{#{ref},
              title={#{title}},
              author={#{author}}
              }
             """ }
  end
end
