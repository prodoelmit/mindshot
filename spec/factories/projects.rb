FactoryGirl.define do
  factory :project do
    owner nil
    name { Faker::Hipster.unique.sentence(2) }
    share_link { Faker::Cat.breed }
    is_shared_with_link false
  end
end
