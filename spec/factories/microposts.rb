require Rails.root.join("lib/parser/parser.rb")
FactoryGirl.define do
  factory :micropost do
    transient do 
      normal_tex false
    end

    tex {
      if normal_tex 
        File.read('spec/fixtures/files/tex.tex')
      else
        Faker::Hipster.unique.paragraph
      end 
    }
    html { 
        "<p>#{tex}</p>" 
    }
    content_path "test.tar.gz"

    after :build do |m|
      if m.tex.include? "\\"
        p m.tex


        path = 'spec/fixtures/files/test.tar.gz'
        file = Rack::Test::UploadedFile.new(path, 'application/gzip', true)
        file_saved = m.save_file(file)
        
        tex = m.tex
        th = TexConverter::TexHandler.new
        th.relative_path = "/system/micropost_contents/#{m.content_path}"
        th.user_id = m.user.id
        parseResult = th.texToHtml tex 

        s = parseResult[:status]
        if s.empty?
            html = parseResult[:tree]
            m.html = html
        end

      end

      
    end
  end
end
