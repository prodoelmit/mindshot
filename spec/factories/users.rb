# spec/factories/users.rb
FactoryGirl.define do
    factory :user do
        email { Faker::Internet.unique.email }
        password "somethinglong"
        authentication_token "zE-nSiJNdGwBz7aXUBn2"
        username { Faker::Internet.unique.user_name(nil, %w( - ) )  }

#        after(:create) do |user, evaluator|
#            FactoryGirl.create(:project, owner: user)
#        end
    end
end
