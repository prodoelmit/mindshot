require 'rails_helper'
describe "POSTing microposts" do
  context "user is signed in" do

    before(:each) do
      @user = do_sign_in
    end

    let(:file) { File.new(Rails.root.join('spec/fixtures/files/test.tar.gz')) }



    context "POSTing to own project", type: :request do 
      context "given one user's post, which is default one" do 
        it "should create micropost in current project, which is default" do
          checkit
        end
      end
      context "given two user's post, but the current is the default one" do
        it "should create micropost in current project, which is default" do
          another_project = create_project(owner: @user)
          checkit
        end
      end
      context "given two user's post and the second as current" do
        it "should create micropost in current project, which is default" do
          another_project = create_project(owner: @user)
          expect(another_project.owner).to be == @user
          @user.current_project = another_project
          expect(@user.current_project).to be == (another_project)
          checkit
        end
      end
      it "should create micropost in current project, which is other user's post, if it's manageable" do
        user2 = FactoryGirl.create(:user)
        p = user2.own_projects.first
        expect(p).to_not be(nil)
        p.add_manager @user
        @user.current_project = p
        expect(@user.current_project.owner).to be == (user2)
        checkit
      end
      it "should create micropost in current project, which is other user's post, if it's writeable" do
        user2 = FactoryGirl.create(:user)
        p = user2.own_projects.first
        p.add_writer @user
        @user.current_project = p
        expect(@user.current_project.owner).to be == (user2)
        checkit
      end



    end
  end
end
