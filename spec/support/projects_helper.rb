module ProjectsHelper
    def create_project(owner: , opts:{})
        p = FactoryGirl.build(:project, opts)
        p.owner = owner
        p.save!
        p
    end
end
