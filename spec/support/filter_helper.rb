module FilterHelper

  def filter query 
    fill_in "Filter", with: query
    click_button "Apply"
  end
end
