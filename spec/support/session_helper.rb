module SessionHelper

    def create_test_user
        email = "test@test.org"
        password = "testtest"
        username = "test-user"
        User.new(username: username, email: email, password: password, password_confirmation: password, authentication_token: "zE-nSiJNdGwBz7aXUBn2").save!
        return username, email, password
    end

    def do_sign_in
        user = FactoryGirl.create(:user)
        login_as(user, scope: :user)
        return user
    end
end
