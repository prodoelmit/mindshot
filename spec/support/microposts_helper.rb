module MicropostsHelper
  def prepare_post user
    headers = { "X-User-Email" => user.email, "X-User-Token" => user.authentication_token }

    tex = file_fixture("tex.tex").read

    data = { micropost: {tex: tex, content_archive: Rack::Test::UploadedFile.new('spec/fixtures/files/test.tar.gz', 'application/gzip', true) }, user_email: user.email, user_token: user.authentication_token }

    return data, headers
  end

  def checkit
              data, headers = prepare_post @user
              expect do
                post microposts_path, params: data
              end.to change{ Micropost.count }.by(1).and change{ @user.current_project.microposts.count }.by(1)
  end
end
