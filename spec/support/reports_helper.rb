module ReportsHelper
    def create_report(owner: , opts:{})
        p = FactoryGirl.build(:report, opts)
        p.owner = owner
        p.save!
        p
    end
end
