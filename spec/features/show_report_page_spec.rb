require 'rails_helper'

describe "Show Report page" do

    describe "if I'm not signed in " do
        before :each do 
            user = FactoryGirl.create(:user)
            p = create_report(owner: user)
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit report_path(p)
        end
        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end
    describe "if I'm signed in" do
        before :each do 
            @user = do_sign_in
            user2 = FactoryGirl.create(:user)
            user3 = FactoryGirl.create(:user)
            user4 = FactoryGirl.create(:user)
            user5 = FactoryGirl.create(:user)
            @name = Faker::Cat.name
            opts = {name: @name}
            @p_read = create_report(owner: user2, opts: opts)
            @p_read.add_reader @user
            @p_write = create_report(owner: user3, opts: opts)
            @p_write.add_writer @user
            @p_manage = create_report(owner: user4, opts: opts)
            @p_manage.add_manager @user
            @p_forbidden = create_report(owner: user5, opts: opts)
            @p_own = create_report(owner: @user, opts: opts)
        end

        [:p_read, :p_write, :p_manage].each do |p_sym|
            it "should have various information and links on #{p_sym}" do
                puts p_sym
                p = eval("@#{p_sym.to_s}", binding)
                visit report_path(p)
                expect(page).to have_content(p.name)
                expect(page).to have_selector(".report-name")
                expect(page).to have_content(@name)
                expect(page).to have_link("@#{p.owner.username}", user_path(username: p.owner.username))
                expect(page).to have_link("Leave report", href: leave_report_path(p))
                expect(page).to have_link("Microposts", href: report_microposts_path(p))
            end
        end


        it "should return 404 when visiting forbidden report" do
            visit report_path(@p_forbidden)
            expect(page.status_code).to eq(404)
        end


        [:p_own, :p_manage].each do |p_sym|
            it "should have 'Manage shares' and 'Edit' buttons on #{p_sym}" do
                p = eval("@#{p_sym.to_s}", binding)
                visit report_path(p)
                expect(page).to have_link("Share settings", href: report_sharesettings_path(p) )
                expect(page).to have_link("Edit", href: edit_report_path(p))
            end
            
            it "should have 'Make current report' button on #{p_sym}", js:true do
                p = eval("@#{p_sym.to_s}", binding)
                visit report_path(p)
                if (@user.current_report == p) 
                    expect(page).to have_selector(".report-is-current-indicator.is-current")
                else
                    expect(page).to have_selector(".report-is-current-indicator.is-not-current")
                    click_link 'Make current'
                    expect(page).to have_selector(".report-is-current-indicator.is-current")
                    visit report_path(p)
                    expect(page).to have_selector(".report-is-current-indicator.is-current")
                end
            end
        end

        [:p_own].each do |p_sym|
            it "shouldn't have 'Leave it' button" do
                p = eval("@#{p_sym.to_s}", binding)
                visit report_path(p)
                expect(page).to have_no_link("Leave report", href: leave_report_path(p) )
            end
        end



        [:p_read, :p_write, :p_manage].each do |p_sym|
            it "should leave report after clicking 'Leave it' on #{p_sym}" do
                p = eval("@#{p_sym.to_s}", binding)
                visit report_path(p)
                expect(page).to have_current_path(report_path(p))
                click_link 'Leave report'
                visit report_path(p)
                expect(page.status_code).to be(404)
            end
        end



    end
end
