require "rails_helper"

describe "Show micropost page" do
  it "should be inaccessible for unauthorized user" do
    u = FactoryGirl.create(:user)
    p = create_project(owner: u)
    m = FactoryGirl.create(:micropost, user: u, project: p)
    current_driver = Capybara.current_driver
    begin
      Capybara.current_driver = :rack_test
      page.driver.submit :delete, "/users/sign_out", {}
    ensure
      Capybara.current_driver = current_driver
    end
    visit micropost_path(m)
    expect(page).to have_current_path(root_path)
  end

  it "should be inaccessible if micropost's project is not shared with you" do
    u0 = do_sign_in
    u1 = FactoryGirl.create(:user)
    p1 = u1.own_projects.first
    m1 = FactoryGirl.create(:micropost, user: u1, project: p1)

    visit micropost_path(m1)
    expect(page.status_code).to be == 404
  end

  context "accesible micropost" do
    before :each do 
      @u = do_sign_in
      @p = @u.own_projects.first
      @m = FactoryGirl.create(:micropost, normal_tex: true, user: @u, project: @p)
      visit micropost_path(@m)
    end

    it "should have project link" do
      expect(page).to have_link(@p.name, href: project_path(@p))
    end

    it "should have author's username" do
      expect(page).to have_content("Author: @#{@u.username}")
      expect(page).to have_link("@#{@u.username}", microposts_path(filter: "user:#{@u.username}"))
    end

    it "should have equations" do
      expect(page).to have_selector(".equation")
      expect(page).to have_selector(".equationstar")
      expect(page).to have_selector(".gather")
      expect(page).to have_selector(".gatherstar")
      expect(page).to have_selector(".align")
      expect(page).to have_selector(".alignstar")
    end

    it "should have code" do
      expect(page.all("pre.prettyprint").count).to be == 4
    end

    it "should have image" do 
      expect(page).to have_selector(".cImg")
    end

    it "should have cite", focus: true do 
      expect(page).to have_link("chen1999applying", href: project_publication_path(project_id: @p, id: "chen1999applying") )
      expect(page).to have_content("[chen1999applying]")
    end

  end
end
