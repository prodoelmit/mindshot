require 'rails_helper'
describe "Edit project page" do
    describe "if I'm not signed in" do
        before :each do 
            user = FactoryGirl.create(:user)
            project = create_project(owner: user)
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit edit_project_path(project)
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do

      before :each do
        @user = do_sign_in
        @git_url = Faker::Internet.url
        @name = Faker::Cat.name
        @description = Faker::Cat.registry
        @git_url2 = Faker::Internet.url
        @name2 = Faker::Cat.name
        @description2 = Faker::Cat.registry
        opts = {name: @name, git_url: @git_url, description: @description}
        @project = create_project(owner: @user, opts: opts)
        visit edit_project_path(@project)
      end


      it{expect(find_field('Name').value).to  be == (@name)}
      it{expect(find_field('Git url').value).to  be == (@git_url)}
      it{expect(find_field('Description').value).to be ==(@description)}

      it "should properly save changed values" do
        fill_in 'Name', with: @name2
        fill_in 'Git url', with: @git_url2
        fill_in 'Description', with: @description2
        click_button 'Save'
        expect(page).to have_current_path(project_path(@project))
        expect(page).to have_content("successfully updated")
        visit edit_project_path(@project)
        expect(find_field('Name').value).to be == (@name2)
        expect(find_field('Git url').value).to be == (@git_url2)
        expect(find_field('Description').value).to be == (@description2)
      end

    end


end
