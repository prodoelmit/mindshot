require 'rails_helper'
describe "Edit micropost page" do
    describe "if I'm not signed in" do
        before :each do 
            user = FactoryGirl.create(:user)
            project = create_project(owner: user)
            mp = FactoryGirl.create(:micropost, user: user, project: project)
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit edit_micropost_path(mp)
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    context "Changing project" do
        before :each do 
            @u1 = do_sign_in
            @p1 = @u1.own_projects.first
            @p2 = create_project(owner: @u1)
            @m1 = FactoryGirl.create(:micropost, user: @u1, project: @p1)
            visit edit_micropost_path(@m1)
        end

        it "should have project selector" do 
            expect(page).to have_select("micropost[project_id]", selected: @p1.name)
            expect(page).to have_button "Save"
        end


        it "should be possible to change project" do 
            select @p2.name, from: "micropost[project_id]"
            click_button "Save"
            visit edit_micropost_path(@m1)
            expect(page).to have_select("micropost[project_id]", selected: @p2.name)
        end

    end
end
