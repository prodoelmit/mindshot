require 'rails_helper'

def report_pane(page, report)
  page.find(".report#id#{report.id}")
end

describe "Reports page" do
    describe "if I'm not signed in" do
        before :each do 
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit reports_path
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do
        before :each do 
            @user = do_sign_in
            user2 = FactoryGirl.create(:user)
            user3 = FactoryGirl.create(:user)
            user4 = FactoryGirl.create(:user)
            p_read = create_report(owner: user2)
            p_read.add_reader @user
            @p_write = create_report(owner: user3)
            @p_write.add_writer @user
            p_manage = create_report(owner: user4)
            p_manage.add_manager @user
            visit reports_path
            
        end


        it {expect(page).to have_content("My reports")}
        it {expect(page).to have_content("Reports shared with me")}

        it "should display user's default report" do
            expect(page).to have_content(@user.own_reports.first.name)
        end


        it "should be possible to go to create new report link" do
            click_link("Create report")
            expect(page).to have_current_path(new_report_path)
        end


        it "each own report should have privacy or access indicator" do
            all('.report.own-report').each do |p|
                expect(p).to have_selector('.report-privacy-indicator')
            end
            all('.report.manageable-report').each do |p|
                expect(p).to have_selector('.report-access-indicator.report-access-indicator-manageable')
            end
            all('.report.writeable-report').each do |p|
                expect(p).to have_selector('.report-access-indicator.report-access-indicator-writeable')
            end
            all('.report.readable-report').each do |p|
                expect(p).to have_selector('.report-access-indicator.report-access-indicator-readable')
            end
        end

        it "One report should be current, others - not" do
          items = []
          items << page.all(".report.writeable-report")
          items << page.all(".report.manageable-report")
          items << page.all(".report.own-report")

          currents = 0 
          not_currents = 0
          total = 0
          items.each do |result|
            result.each do |item|
              within item do
                currents += item.find_all(".report-is-current-indicator.is-current").count
                not_currents += item.find_all(".report-is-current-indicator.is-not-current").count
                total += 1
              end
            end
          end

          expect(not_currents).to be == (total - 1)
          expect(currents).to be == 1
        end

        it "should be possible to switch current report clicking link", js: true do
          item_default = report_pane(page, @user.current_report)
          item_write = report_pane(page, @p_write)

          expect(item_default).to have_selector(".report-is-current-indicator.is-current")
          expect(item_write).to have_selector(".report-is-current-indicator.is-not-current")

          within item_write do
            click_link 'Make current'
          end

          expect(item_default).to have_selector(".report-is-current-indicator.is-not-current")
          expect(item_write).to have_selector(".report-is-current-indicator.is-current")

          visit reports_path

          item_default = report_pane(page, @user.current_report)
          item_write = report_pane(page, @p_write)
          expect(item_default).to have_selector(".report-is-current-indicator.is-not-current")
          expect(item_write).to have_selector(".report-is-current-indicator.is-current")


        end



    end
end
