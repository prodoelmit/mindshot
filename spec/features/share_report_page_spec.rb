require 'rails_helper'
require Rails.root.join('app/models/user_report.rb')

describe "Share Report page" do
  describe "if I'm not signed in " do
    before :each do 
      user = FactoryGirl.create(:user)
      p = create_report(owner: user)
      current_driver = Capybara.current_driver
      begin
        Capybara.current_driver = :rack_test
        page.driver.submit :delete, "/users/sign_out", {}
      ensure
        Capybara.current_driver = current_driver
      end
      visit report_sharesettings_path(p)
    end
    it "should redirect me to root page and ask to sign_in" do
      expect(page).to have_current_path(root_path)
    end
  end
  describe "if I'm signed in" do
    before :each do 
      @user = do_sign_in
      user2 = FactoryGirl.create(:user)
      user3 = FactoryGirl.create(:user)
      user4 = FactoryGirl.create(:user)
      @user5 = FactoryGirl.create(:user)
      @user6 = FactoryGirl.create(:user)
      @p_read = create_report(owner: user2)
      @p_read.add_reader @user
      @p_write = create_report(owner: user3)
      @p_write.add_writer @user
      @p_manage = create_report(owner: user4)
      @p_manage.add_manager @user
      @p_forbidden = create_report(owner: @user5)
      @p_own = create_report(owner: @user)
    end

    [:p_read, :p_write, :p_forbidden].each do |p_sym|
      it "should redirect to reports#show on attempt to change share settings on #{p_sym}" do
        p = eval("@#{p_sym.to_s}", binding)
        visit report_sharesettings_path(p)
        expect(page).to have_current_path(report_path(p))
      end
    end

    [:p_manage, :p_own].each do |p_sym|
      it "should be reports#sharesettings when visiting #{p_sym}" do
        p = eval("@#{p_sym.to_s}", binding)
        visit report_sharesettings_path(p)
        expect(page).to have_current_path(report_sharesettings_path(p))
        expect(page).to have_content "Share settings"
        expect(page).to have_field "Username"
        expect(page).to have_button "Add user"
      end

      describe "with javascript", js: true do

        it "should add user when clicking 'Add user'" do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          expect do
            fill_in "Username", with: @user5.username
            click_button "Add user"
          end.to change{ page.find_all("#share-user-list > .share-user").count }.by(1)
          item = find_all("#share-user-list > .share-user").last
          expect(item).to have_select("level", selected: "Can read")
          expect(item).to have_content(@user5.username)
          expect(item).to have_link("Kick")
        end

        it "shouldn't have 'Save' button" do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          expect(page).to have_no_button("Save")
        end

        it "should have 'Kick' link " do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          items = all("#share-user-list > .share-user")
          items[1...items.size].each do |i|
            hasJs = i.find("#hasJs")
            expect(hasJs).to have_link("Kick")
          end
        end

        it "should change permissions upon choosing permission in <select> and refresh", :focus do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          fill_in "Username", with: @user5.username
          click_button "Add user"
          item = all("#share-user-list > .share-user").last.find("#hasJs")
          within item do
            select "Can write", from: "level"
          end
          expect(item).to have_select("level", selected: "Can write")
          visit report_sharesettings_path(p)
          p "Hi"
          visit report_sharesettings_path(p)
          p "Hi"
          item = find_all("#share-user-list > .share-user").last
          expect(item).to have_select("level", selected: "Can write")
        end

        it "should be possible to kick user", focus: true do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p) 
          fill_in "Username", with: @user5.username
          click_button "Add user"
          items = find_all("#share-user-list > .share-user")
          expect do
            items.last.find("#hasJs").click_link "Kick"
          end.to change{ find_all("#share-user-list > .share-user").count }.by(-1)


        end

      end

context "without javascript", js: false do

        it "should add user when clicking 'Add user' ", js:false do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          expect do
            fill_in "Username", with: @user5.username
            click_button "Add user"
          end.to change{ page.find_all("#share-user-list > .share-user").count }.by(1)
          item = find_all("#share-user-list > .share-user").last
          expect(item).to have_select("level", selected: "Can read")
          expect(item).to have_content(@user5.username)
        end


        it "should change permissions upon choosing permission in <select> and clicking Save " do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          fill_in "Username", with: @user5.username
          click_button "Add user"
          item = all("#share-user-list > .share-user").last.find("noscript")
          within item do
            select "Can write", from: "level"
            click_button "Save"
          end
          item = find_all("#share-user-list > .share-user").last.find("noscript")
          expect(item).to have_select("level", selected: "Can write")
        end

        it "should have 'Kick' link " do
          p = eval("@#{p_sym.to_s}", binding)
          visit report_sharesettings_path(p)
          items = all("#share-user-list > .share-user")
          items[1...items.size].each do |i|
            noscript = i.find("noscript")
            expect(noscript).to have_link("Kick")
          end
        end


      end

    end

    it "should have myself on p_manage shares list" do
      visit report_sharesettings_path(@p_manage)
      item = find("#share-user-list")
      expect(item).to have_content @user.username
    end




  end
end


