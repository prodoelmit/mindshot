require 'rails_helper'

def create_3_microposts
  user1.own_projects.first.add_reader @user
  user2.own_projects.first.add_writer @user
  data, headers = prepare_post @user
  FactoryGirl.create(:micropost, user: user1, project: user1.current_project)
  FactoryGirl.create(:micropost, user: user2, project: user2.current_project)
  FactoryGirl.create(:micropost, user: @user, project: @user.current_project)
  visit microposts_path
end
describe "Microposts page" do
  include RSpec::Rails::ControllerExampleGroup
  it "should be inaccessible for unauthorized user" do
    current_driver = Capybara.current_driver
    begin
      Capybara.current_driver = :rack_test
      page.driver.submit :delete, "/users/sign_out", {}
    ensure
      Capybara.current_driver = current_driver
    end
    visit microposts_path
    expect(page).to have_current_path(root_path)
  end
  context "user is signed in" do
    before :each do 
      @user = do_sign_in
      visit microposts_path
    end

    let(:user1) {FactoryGirl.create(:user)}
    let(:user2) {FactoryGirl.create(:user)}
    let(:user3) {FactoryGirl.create(:user)}


    it "should say something good if there are no microposts yet" do
      expect(page).to have_content("No microposts found")
    end

    it "should have filter field" do
      expect(page).to have_field("Filter")
      expect(page).to have_button("Apply")
    end

    context "Filtering" do
      before(:each) do
        @u0 = @user
        @u1 = user1
        @u2 = user2
        @u3 = user3

        @p0 = @u0.own_projects.first
        @p1 = @u1.own_projects.first
        @p2 = @u2.own_projects.first
        @p3 = @u3.own_projects.first

        @p1.add_reader @u0
        @p2.add_writer @u0
        #don't add @u0 to @p3

        @m0 = FactoryGirl.create(:micropost, user: @u0, project: @p0)
        @m1 = FactoryGirl.create(:micropost, user: @u1, project: @p1)
        @m2 = FactoryGirl.create(:micropost, user: @u2, project: @p2)
        @m3 = FactoryGirl.create(:micropost, user: @u3, project: @p3)

        visit microposts_path
      end

      it "should show microposts from readable projects when filter is empty ", focus: true do
        filter " "
        expect(page.all(".micropost").count).to be == 3
        for p in [@p0, @p1, @p2] do 
          expect(page).to have_content(p.microposts.first.tex)
        end

        expect(page).to have_no_content(@p3.microposts.first.tex)
      end


      it "should show microposts from filtered project(s) when filtering by id" do
        for p in [@p0, @p1, @p2] do 
          filter "project:##{p.id}"
          expect(page.all(".micropost").count).to be == 1
          expect(page).to have_content(p.microposts.first.tex)
        end
        for (p1, p2) in [[@p0, @p1], [@p0, @p2], [@p1, @p2]] do 
          filter "project:##{p1.id}, ##{p2.id}"
          expect(page.all(".micropost").count).to be == 2
          expect(page).to have_content(p1.microposts.first.tex)
          expect(page).to have_content(p2.microposts.first.tex)
        end
      end

      it "shouldn't show any microposts from unreadable project", focus: true do
        filter "project:##{@p3.id}" 
        expect(page.all(".micropost").count).to be == 0
      end

      it "should show microposts by filtered user(s) when filtered by username" do
        for u in [@u0, @u1, @u2]
          filter "user:#{u.username}" 
          expect(page.all(".micropost").count).to be == 1
          expect(page).to have_content(u.microposts.first.tex)
        end
        for (u1, u2) in [[@u0, @u1], [@u0, @u2], [@u1, @u2]] do
          filter "user:#{u1.username}, #{u2.username}" 
          expect(page.all(".micropost").count).to be == 2
          expect(page).to have_content(u1.microposts.first.tex)
          expect(page).to have_content(u2.microposts.first.tex)
        end
      end 

    end
  end
end
