require 'rails_helper'
describe "Show Publication page" do
  describe "if I'm not signed in " do
    before :each do 
      user = FactoryGirl.create(:user)
      p = create_project(owner: user)
      pub = FactoryGirl.create(:publication, user: user, project: p)
      current_driver = Capybara.current_driver
      begin
        Capybara.current_driver = :rack_test
        page.driver.submit :delete, "/users/sign_out", {}
      ensure
        Capybara.current_driver = current_driver
      end
      visit publication_path(p)
    end
    it "should redirect me to root page and ask to sign_in" do
      expect(page).to have_current_path(root_path)
    end
  end

  describe "if I'm signed in" do
    before :each do
      @user0 = do_sign_in
      @user1 = FactoryGirl.create(:user)
      @user2 = FactoryGirl.create(:user)
      @user3 = FactoryGirl.create(:user)
      @user4 = FactoryGirl.create(:user)
      @p_own = create_project(owner: @user0)
      @p_read = create_project(owner: @user1)
      @p_write = create_project(owner: @user2)
      @p_manage = create_project(owner: @user3)
      @p_forbidden = create_project(owner: @user4)
      @p_read.add_reader @user0
      @p_write.add_writer @user0
      @p_manage.add_manager @user0
      @pub_own = FactoryGirl.create(:publication, user: @user0, project: @p_own)
      @pub_read = FactoryGirl.create(:publication, user: @user1, project: @p_read)
      @pub_write = FactoryGirl.create(:publication, user: @user2, project: @p_write)
      @pub_manage = FactoryGirl.create(:publication, user: @user3, project: @p_manage)
      @pub_forbidden = FactoryGirl.create(:publication, user: @user4, project: @p_forbidden)
      visit publication_path(@pub_own)
    end

    %i[pub_own pub_read pub_manage pub_write].each do |pub_sym|
      %i[by_ref_project by_id_project by_id_absolute].each do |path_type|
        context "#{pub_sym.to_s} #{path_type.to_s}" do
          before :each do
            @pub = eval("@#{pub_sym.to_s}", binding)
            case path_type
            when :by_ref_project
              visit project_publication_path(project_id: @pub.project.id, id: @pub.ref)
            when :by_id_project
              visit project_publication_path(project_id: @pub.project.id, id: @pub.id)
            when :by_id_absolute
              visit publication_path(@pub)
            end
          end


          it "should have project link", focus: true do
            expect(page).to have_link(@pub.project.name, project_path(@pub.project))
          end

          it "should have ref and bibtex" do
            expect(page).to have_selector("#ref")
            expect(find_field("ref").value).to be == @pub.ref

            expect(page).to have_selector("pre#bibtex")
            expect(find("pre#bibtex")).to have_content(@pub.bibtex)
          end

          it "should have pdf iframe if pdf is uploaded" do
            file = Rack::Test::UploadedFile.new('spec/fixtures/files/example.pdf', 'application/pdf', true)
            @pub.pdf = file
            @pub.save
            visit publication_path(@pub)
            expect(@pub.pdf.present?).to be == true
            expect(page).to have_selector("iframe")
            expect(find("iframe")[:src]).to be == @pub.pdf.url
          end

          it "shouldn't have pdf iframe if there is no pdf" do
            expect(page).to have_no_selector("iframe")
          end

          if pub_sym.in? %i[pub_own pub_manage pub_write] 
            it "should have edit button" do
              expect(page).to have_link("Edit", edit_publication_path(@pub))
            end
          else 
            it "shouldn't have edit button" do
              expect(page).to have_no_link("Edit")
            end
          end
        end
      end
    end

    %i[pub_forbidden].each do |pub_sym|
      %i[by_ref_project by_id_project by_id_absolute].each do |path_type|
        context "#{pub_sym.to_s} #{path_type.to_s}" do
          before :each do
            @pub = eval("@#{pub_sym.to_s}", binding)
            case path_type
            when :by_ref_project
              visit project_publication_path(project_id: @pub.project.id, id: @pub.ref)
            when :by_id_project
              visit project_publication_path(project_id: @pub.project.id, id: @pub.id)
            when :by_id_absolute
              visit publication_path(@pub)
            end
          end

          it "should not find forbidden publication" do 
            case path_type
            when :by_ref_project
              expect(page).to have_content("Publication #{@pub.ref} not found")
            when :by_id_project, :by_id_absolute
              expect(page).to have_content("Publication #{@pub.id} not found")
            end
          end

        end
      end

    end


  end
end
