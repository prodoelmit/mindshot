require 'rails_helper'

describe "Show Project page" do

    describe "if I'm not signed in " do
        before :each do 
            user = FactoryGirl.create(:user)
            p = create_project(owner: user)
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit project_path(p)
        end
        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end
    describe "if I'm signed in" do
        before :each do 
            @user = do_sign_in
            user2 = FactoryGirl.create(:user)
            user3 = FactoryGirl.create(:user)
            user4 = FactoryGirl.create(:user)
            user5 = FactoryGirl.create(:user)
            @git_url = Faker::Internet.url
            @name = Faker::Cat.name
            @description = Faker::Cat.registry
            opts = {name: @name, git_url: @git_url, description: @description}
            @p_read = create_project(owner: user2, opts: opts)
            @p_read.add_reader @user
            @p_write = create_project(owner: user3, opts: opts)
            @p_write.add_writer @user
            @p_manage = create_project(owner: user4, opts: opts)
            @p_manage.add_manager @user
            @p_forbidden = create_project(owner: user5, opts: opts)
            @p_own = create_project(owner: @user, opts: opts)
        end

        [:p_read, :p_write, :p_manage].each do |p_sym|
            it "should have various information and links on #{p_sym}" do
                puts p_sym
                p = eval("@#{p_sym.to_s}", binding)
                visit project_path(p)
                expect(page).to have_content(p.name)
                expect(page).to have_content(p.description)
                expect(page).to have_content(p.git_url)
                expect(page).to have_selector(".project-name")
                expect(page).to have_selector(".project-description")
                expect(page).to have_selector(".project-git-url")
                expect(page).to have_content(@name)
                expect(page).to have_link("@#{p.owner.username}", user_path(username: p.owner.username))
                expect(page).to have_content(@description)
                expect(page).to have_link(@git_url, href: @git_url)
                expect(page).to have_link("Leave project", href: leave_project_path(p))
                expect(page).to have_link("Microposts", href: project_microposts_path(p))
            end
        end


        it "should return 404 when visiting forbidden project" do
            visit project_path(@p_forbidden)
            expect(page.status_code).to eq(404)
        end


        [:p_own, :p_manage].each do |p_sym|
            it "should have 'Manage shares' and 'Edit' buttons on #{p_sym}" do
                p = eval("@#{p_sym.to_s}", binding)
                visit project_path(p)
                expect(page).to have_link("Share settings", href: project_sharesettings_path(p) )
                expect(page).to have_link("Edit", href: edit_project_path(p))
            end
            
            it "should have 'Make current project' button on #{p_sym}", js:true do
                p = eval("@#{p_sym.to_s}", binding)
                visit project_path(p)
                if (@user.current_project == p) 
                    expect(page).to have_selector(".project-is-current-indicator.is-current")
                else
                    expect(page).to have_selector(".project-is-current-indicator.is-not-current")
                    click_link 'Make current'
                    expect(page).to have_selector(".project-is-current-indicator.is-current")
                    visit project_path(p)
                    expect(page).to have_selector(".project-is-current-indicator.is-current")
                end
            end
        end

        [:p_own].each do |p_sym|
            it "shouldn't have 'Leave it' button" do
                p = eval("@#{p_sym.to_s}", binding)
                visit project_path(p)
                expect(page).to have_no_link("Leave project", href: leave_project_path(p) )
            end
        end



        [:p_read, :p_write, :p_manage].each do |p_sym|
            it "should leave project after clicking 'Leave it' on #{p_sym}" do
                p = eval("@#{p_sym.to_s}", binding)
                visit project_path(p)
                expect(page).to have_current_path(project_path(p))
                click_link 'Leave project'
                visit project_path(p)
                expect(page.status_code).to be(404)
            end
        end



    end
end
