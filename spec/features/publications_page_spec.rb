require 'rails_helper'

describe "Publications page" do
  it "should be inaccessible for unauthorized user" do
    current_driver = Capybara.current_driver
    begin
      Capybara.current_driver = :rack_test
      page.driver.submit :delete, "/users/sign_out", {}
    ensure
      Capybara.current_driver = current_driver
    end
    visit publications_path
    expect(page).to have_current_path(root_path)
  end

  context "user is signed in" do 
    before :each do
      @user = do_sign_in
      visit publications_path
    end

    let(:user1) {FactoryGirl.create(:user)}
    let(:user2) {FactoryGirl.create(:user)}
    let(:user3) {FactoryGirl.create(:user)}

    it "should say something good if there are no publications yet" do
      expect(page).to have_content("No publications found")
    end

    it "should have filter field" do
      expect(page).to have_field("Filter")
      expect(page).to have_button("Apply")
    end
    context "Filtering" do
      before(:each) do
        @u0 = @user
        @u1 = user1
        @u2 = user2
        @u3 = user3

        @p0 = @u0.own_projects.first
        @p1 = @u1.own_projects.first
        @p2 = @u2.own_projects.first
        @p3 = @u3.own_projects.first

        @p1.add_reader @u0
        @p2.add_writer @u0
        #don't add @u0 to @p3

        @pub0 = FactoryGirl.create(:publication, user: @u0, project: @p0)
        @pub1 = FactoryGirl.create(:publication, user: @u1, project: @p1)
        @pub2 = FactoryGirl.create(:publication, user: @u2, project: @p2)
        @pub3 = FactoryGirl.create(:publication, user: @u3, project: @p3)

        visit publications_path
      end

      it "should show publications from readable projects when filter is empty " do
        filter " "
        expect(page.all(".publication").count).to be == 3
        for p in [@p0, @p1, @p2] do 
          expect(page).to have_content(p.publications.first.title)
        end

        expect(page).to have_no_content(@p3.publications.first.title)
      end


      it "should show publications from filtered project(s)" do
        for p in [@p0, @p1, @p2] do 
          filter "project:#{p.id}"
          expect(page.all(".publication").count).to be == 1
          expect(page).to have_content(p.publications.first.title)
        end
        for (p1, p2) in [[@p0, @p1], [@p0, @p2], [@p1, @p2]] do 
          filter "project:#{p1.id}, #{p2.id}"
          expect(page.all(".publication").count).to be == 2
          expect(page).to have_content(p1.publications.first.title)
          expect(page).to have_content(p2.publications.first.title)
        end
      end

      it "shouldn't show any publications from unreadable project", focus: true do
        filter "project:##{@p3.id}" 
        expect(page.all(".publication").count).to be == 0
      end

      it "should show publications by filtered user(s) when filtered by username" do
        for u in [@u0, @u1, @u2]
          
          filter "user:#{u.username}" 
          expect(page.all(".publication").count).to be == 1
          expect(page).to have_content(u.publications.first.title)
        end
        for (u1, u2) in [[@u0, @u1], [@u0, @u2], [@u1, @u2]] do
          filter "user:#{u1.username}, #{u2.username}" 
          expect(page.all(".publication").count).to be == 2
          expect(page).to have_content(u1.publications.first.title)
          expect(page).to have_content(u2.publications.first.title)
        end
      end 
    end

  end

end
