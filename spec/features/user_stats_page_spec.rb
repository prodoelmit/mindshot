require 'rails_helper'

describe "User stats page" do
  it "should be inaccessible for unauthorized user" do
    user = FactoryGirl.create(:user)
    current_driver = Capybara.current_driver
    begin
      Capybara.current_driver = :rack_test
      page.driver.submit :delete, "/users/sign_out", {}
    ensure
      Capybara.current_driver = current_driver
    end
    visit user_stats_path(username: user.username)
    expect(page).to have_current_path(root_path)
  end

  context "when signed in" do
    before(:each) do
      @u = do_sign_in
      visit user_stats_path(username: @u.username)

    end

    it "should have caption with username" do
      expect(page).to have_content("Stats of @#{@u.username}")
    end

    it "should have chart area" do
      expect(page).to have_css("#chart")
    end


  end





end
