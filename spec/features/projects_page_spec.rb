require 'rails_helper'

def project_pane(page, project)
  page.find(".project#id#{project.id}")
end

describe "Projects page" do
    describe "if I'm not signed in" do
        before :each do 
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit projects_path
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do
        before :each do 
            @user = do_sign_in
            user2 = FactoryGirl.create(:user)
            user3 = FactoryGirl.create(:user)
            user4 = FactoryGirl.create(:user)
            p_read = create_project(owner: user2)
            p_read.add_reader @user
            @p_write = create_project(owner: user3)
            @p_write.add_writer @user
            p_manage = create_project(owner: user4)
            p_manage.add_manager @user
            visit projects_path
            
        end


        it {expect(page).to have_content("My projects")}
        it {expect(page).to have_content("Projects shared with me")}

        it "should display user's default project" do
            expect(page).to have_content(@user.own_projects.first.name)
        end


        it "should be possible to go to create new project link" do
            click_link("Create project")
            expect(page).to have_current_path(new_project_path)
        end


        it "each own project should have privacy or access indicator" do
            all('.project.own-project').each do |p|
                expect(p).to have_selector('.project-privacy-indicator')
            end
            all('.project.manageable-project').each do |p|
                expect(p).to have_selector('.project-access-indicator.project-access-indicator-manageable')
            end
            all('.project.writeable-project').each do |p|
                expect(p).to have_selector('.project-access-indicator.project-access-indicator-writeable')
            end
            all('.project.readable-project').each do |p|
                expect(p).to have_selector('.project-access-indicator.project-access-indicator-readable')
            end
        end

        it "One project should be current, others - not" do
          items = []
          items << page.all(".project.writeable-project")
          items << page.all(".project.manageable-project")
          items << page.all(".project.own-project")

          currents = 0 
          not_currents = 0
          total = 0
          items.each do |result|
            result.each do |item|
              within item do
                currents += item.find_all(".project-is-current-indicator.is-current").count
                not_currents += item.find_all(".project-is-current-indicator.is-not-current").count
                total += 1
              end
            end
          end

          expect(not_currents).to be == (total - 1)
          expect(currents).to be == 1
        end

        it "should be possible to switch current project clicking link", js: true do
          item_default = project_pane(page, @user.current_project)
          item_write = project_pane(page, @p_write)

          expect(item_default).to have_selector(".project-is-current-indicator.is-current")
          expect(item_write).to have_selector(".project-is-current-indicator.is-not-current")

          within item_write do
            click_link 'Make current'
          end

          expect(item_default).to have_selector(".project-is-current-indicator.is-not-current")
          expect(item_write).to have_selector(".project-is-current-indicator.is-current")

          visit projects_path

          item_default = project_pane(page, @user.current_project)
          item_write = project_pane(page, @p_write)
          expect(item_default).to have_selector(".project-is-current-indicator.is-not-current")
          expect(item_write).to have_selector(".project-is-current-indicator.is-current")


        end



    end
end
