require 'rails_helper'
describe "New project page" do
    describe "if I'm not signed in" do
        before :each do 
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit new_project_path
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do

      before :each do
        @user = do_sign_in
        visit new_project_path
      end

      it {expect(page).to have_current_path(new_project_path)}
      it {expect(page).to have_content("New project")}
      it {expect(page).to have_field("Name")}
      it {expect(page).to have_field("Git url")}
      it {expect(page).to have_field("Description")}
      it {expect(page).to have_button("Create")}

      it "should create project when name is filled" do
        fill_in "Name", with: Faker::Food.ingredient
        expect do
          click_button "Create"
        end.to change{@user.own_projects.count}.by(1)
      end

    end


end
