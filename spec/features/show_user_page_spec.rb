require 'rails_helper'

describe "Show user page" do 
  it "should be inaccessible for unauthorized user" do
    user = FactoryGirl.create(:user)
    current_driver = Capybara.current_driver
    begin
      Capybara.current_driver = :rack_test
      page.driver.submit :delete, "/users/sign_out", {}
    ensure
      Capybara.current_driver = current_driver
    end
    visit user_path(username: user.username)
    expect(page).to have_current_path(root_path)
  end

  context "when signed in" do
    before :each do 
      @u0 = do_sign_in
      @p0 = @u0.current_project

      @u1 = FactoryGirl.create(:user)
      @p1 = @u1.current_project
      @p1.add_reader @u0

      @p0.add_reader @u1

      @p2 = create_project(owner: @u1)

      @u2 = FactoryGirl.create(:user)
      @p3 = create_project(owner: @u2)
      @p3.add_reader @u1


      visit user_path(username: @u1.username)
    end

    it "should have links to readable projects" do
      save_and_open_page
      expect(page).to have_content "Own projects"
      expect(page).to have_link(nil, href:project_path(@p1))
      expect(page).to have_no_link(nil, href:project_path(@p2))

      expect(page).to have_content "Participates in projects" 
      expect(page).to have_link(nil, href: project_path(@p0))
      expect(page).to have_no_link(nil, href: project_path(@p3))
    end

    it "should have stats chart" do
      expect(page).to have_css("#chart")
    end

    it "should have link to microposts" do 
      expect(page).to have_link("Microposts", href: user_microposts_path(username: @u1.username))
    end

    

  end


end
