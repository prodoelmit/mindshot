require 'rails_helper'
describe "Home page" do
    describe "if I'm not signed in" do
        before :each do 
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit root_path
        end

        it "shouldn't redirect us anywhere" do
            expect(page).to have_current_path(root_path)
        end


        it "should have login form, register and forgot password buttons" do 
            expect(page).to have_link("Sign up", href: new_user_registration_path)
            expect(page).to have_field("Email")
            expect(page).to have_field("Password")
            expect(page).to have_button("Sign in")
            expect(page).to have_link("Forgot your password?")
        end

        it "should sign me in if I enter right credentials" do
            username, email, password = create_test_user
            visit new_user_session_path
            fill_in "Email", with: email
            fill_in "Password", with: password
            click_button "Sign in"

            expect(page).to have_content("Signed in successfully")
            expect(page).to have_current_path(root_path)
        end

        it "shouldn't sign me in if I enter wrong credentials" do
            email = 'notexistanttestemail@test.org'
            password = 'test'

            fill_in "Email", with: email
            fill_in "Password", with: password
            click_button "Sign in"

            expect(page).to have_content("Invalid")
            expect(page).to have_current_path(root_path)
        end

        it "shouldn't have link to microposts" do 
            expect(page).not_to have_link("Microposts", href: microposts_path)
        end
        it "shouldn't have link to publications" do 
            expect(page).not_to have_link("Publications", href: publications_path)
        end
        it "shouldn't have link to settings" do 
            expect(page).not_to have_link("Settings", href: settings_path)
        end
        it "shouldn't have log out link" do
            expect(page).not_to have_link("Log out", href: destroy_user_session_path)
        end
        it "shouldn't have link to projects" do
            expect(page).not_to have_link("Projects", href: projects_path)
        end
    end

    describe "if I'm signed in" do
        before :each do
            do_sign_in
            visit root_path
        end

        it "should be dashboard page" do
            expect(page).to have_content("Dashboard")
        end

        it "should have link to microposts" do 
            expect(page).to have_link("Microposts", href: microposts_path)
        end
        it "should have link to publications" do 
            expect(page).to have_link("Publications", href: publications_path)
        end
        it "should have link to settings" do 
            expect(page).to have_link("Settings", href: settings_path)
        end
        it "should have link to projects" do 
            expect(page).to have_link("Projects", href: projects_path)
        end
        it "should have log out link" do
            expect(page).to have_link("Log out", href: destroy_user_session_path)
        end

        it "should log me out upon clicking on log out button" do
            click_link "Log out"
            expect(page).to have_content("Signed out successfully")
        end
    end
end
