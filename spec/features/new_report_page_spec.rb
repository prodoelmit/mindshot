require 'rails_helper'
describe "New report page" do
    describe "if I'm not signed in" do
        before :each do 
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit new_report_path
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do

      before :each do
        @user = do_sign_in
        visit new_report_path
      end

      it {expect(page).to have_current_path(new_report_path)}
      it {expect(page).to have_content("New report")}
      it {expect(page).to have_field("Name")}
      it {expect(page).to have_button("Create")}

      it "should create report when name is filled" do
        fill_in "Name", with: Faker::Food.ingredient
        expect do
          click_button "Create"
        end.to change{@user.own_reports.count}.by(1)
      end

    end


end
