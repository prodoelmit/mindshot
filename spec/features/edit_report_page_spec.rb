require 'rails_helper'
describe "Edit report page" do
    describe "if I'm not signed in" do
        before :each do 
            user = FactoryGirl.create(:user)
            report = create_report(owner: user)
            current_driver = Capybara.current_driver
            begin
                Capybara.current_driver = :rack_test
                page.driver.submit :delete, "/users/sign_out", {}
            ensure
                Capybara.current_driver = current_driver
            end
            visit edit_report_path(report)
        end

        it "should redirect me to root page and ask to sign_in" do
            expect(page).to have_current_path(root_path)
        end
    end

    describe "if I'm signed in" do

      before :each do
        @user = do_sign_in
        @name = Faker::Cat.name
        @name2 = Faker::Cat.name
        opts = {name: @name} 
        @report = create_report(owner: @user, opts: opts)
        visit edit_report_path(@report)
      end


      it{expect(find_field('Name').value).to  be == (@name)}

      it "should properly save changed values" do
        fill_in 'Name', with: @name2
        click_button 'Save'
        expect(page).to have_current_path(report_path(@report))
        expect(page).to have_content("successfully updated")
        visit edit_report_path(@report)
        expect(find_field('Name').value).to be == (@name2)
      end

    end


end
