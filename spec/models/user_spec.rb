# spec/models/user_spec.rb
require 'rails_helper'

describe User do 
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:password) }
    it { is_expected.to validate_confirmation_of(:password).on(:create) }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_uniqueness_of(:username_lowercase) }
    it { is_expected.to have_valid(:username).when('prodoelmit123', 'p-s-lkjasd', 'sOmELongName', 'AnotherLongName') } 
    it { is_expected.to_not have_valid(:username).when('_StartingWithUnderscore', 'Name.with.dots', 'NamewithsomeAsterisk*', 'NameWith&Ampersand', 'NameWithHyphenAtEnd-', '-HyphenAtStart') } 


    it "should have default project after creation" do
        user = FactoryGirl.create(:user)
        expect(user.own_projects.count).to be(1)
    end

    it "should be able to leave projects" do
        user1, user2 = FactoryGirl.create_pair(:user) 
        p = create_project(owner: user1)


            p.add_reader user2
            expect do
                user2.leave_project(p)
            end.to change{user2.projects.count}.from(1).to(0)

            p.add_writer user2
            expect do
                user2.leave_project(p)
            end.to change{user2.projects.count}.from(1).to(0)

            p.add_manager user2
            expect do
                user2.leave_project(p)
            end.to change{user2.projects.count}.from(1).to(0)

    end

    it "should be able to read readable projects" do
        user1, user2 = FactoryGirl.create_pair(:user) 
        p = create_project(owner: user1)
        p.add_reader user2
        expect(user2.can_read(p)).to be(true)
    end

    it "should be able to read readable projects" do
        user1, user2 = FactoryGirl.create_pair(:user) 
        p = create_project(owner: user1)
        p.add_writer user2
        expect(user2.can_write(p)).to be(true)
    end

    it "should be able to read readable projects" do
        user1, user2 = FactoryGirl.create_pair(:user) 
        p = create_project(owner: user1)
        p.add_manager user2
        expect(user2.can_manage(p)).to be(true)
    end
end
