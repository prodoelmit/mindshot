require 'rails_helper'

describe Publication do
  context 'validations' do
    it {is_expected.to validate_presence_of :project }
    it {is_expected.to validate_presence_of :user }
    it {is_expected.to validate_presence_of :bibtex }
    it {is_expected.to validate_presence_of :ref }
    it {is_expected.to validate_presence_of :author }
    it {is_expected.to validate_presence_of :title }
    it {is_expected.to validate_uniqueness_of(:ref).scoped_to(:project_id)}
  end

  it "should be possible to copy it to another project" do
      @u1 = FactoryGirl.create(:user)
      @p1 = create_project(owner: @u1)
      @u2 = FactoryGirl.create(:user)
      @p2 = create_project(owner: @u2)
      @pub = FactoryGirl.create(:publication, project_id: @p1.id, user_id: @u1.id)

    expect(@u1.publications.count).to be == 1
    expect(@p1.publications.count).to be == 1

    expect do
      newpub = @pub.copy_to_project(@p2)
      expect(newpub.bibtex).to be == @pub.bibtex
      expect(newpub.title).to be == @pub.title
      expect(newpub.author).to be == @pub.author
      expect(newpub.ref).to be == @pub.ref
    end.to change{ @u1.publications.count }.by(1).and change{@p2.publications.count}.by(1)
  end







end

