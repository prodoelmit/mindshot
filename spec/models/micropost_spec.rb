# spec/models/micropost_spec.rb
require 'rails_helper'

describe Micropost do
    include Rack::Test::Methods
    include ActionDispatch::TestProcess

    context 'validations' do 
        it { is_expected.to validate_presence_of :tex   }
        it { is_expected.to validate_presence_of :html   }
        it { is_expected.to validate_presence_of :content_path   }
        it { is_expected.to validate_presence_of :project }
    end


    describe '#save_file' do
        before(:all) do
            @mp = Micropost.new
            path = 'spec/fixtures/files/test.tar.gz'
            file = Rack::Test::UploadedFile.new(path, 'application/gzip', true)
            @result = @mp.save_file(file)
        end
        it 'untars file' do
            expect(@result).to be(true)
        end
        it 'creates directory accessible with #full_content_path' do
            expect(@mp.full_content_path).to be_directory
        end
        it 'contains "figures" directory' do 
            expect(@mp.full_content_path.join("figures")).to be_directory
        end
        it 'contains "tex.tex" file' do
            expect(@mp.full_content_path.join("tex.tex")).to exist
        end
    end

end
