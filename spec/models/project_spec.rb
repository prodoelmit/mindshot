require 'rails_helper'

describe Project do 
    let(:user) { FactoryGirl.create(:user) }
    let(:user2) {FactoryGirl.create(:user)}
    it { is_expected.to validate_presence_of(:owner) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:share_link) } 

    it "should be owned by owner" do
        expect do 
            p = create_project(owner: user)
        end.to change{user.own_projects.count}.by 1
    end
    it "should be able to have microposts" do
        p = create_project(owner: user)
        expect do
            mp = FactoryGirl.build(:micropost) do |m|
                m.user_id =  user.id
                m.project_id = p.id
            end
            mp.save!
        end.to change{p.microposts.count}.by(1)
    end

    it "should be shareable between users with read-only permissions" do
        p = create_project(owner: user)
        expect do
            p.add_reader user2
        end.to change{p.readers.count}.by(1)

    end

    it "should be able to kick reader" do
        p = create_project(owner: user)
        p.add_reader user2
        expect do
            p.kick user2
        end.to change{p.readers.count}.by(-1)
    end

    it "should be shareable between users with read-write permissions" do 
        p = create_project(owner: user)
        expect do
            p.add_writer user2
        end.to change{p.writers.count}.by(1)
    end

    it "should be able to kick writer" do
        p = create_project(owner:user)
        p.add_writer user2
        expect do
            p.kick user2
        end.to change{p.writers.count}.by(-1)
    end

    it "should be shareable between users with manage permissions" do 
        p = create_project(owner: user)
        expect do
            p.add_manager user2
        end.to change{p.managers.count}.by(1)
    end

    it "should be able to kick manager" do
        p = create_project(owner: user)
        p.add_manager user2
        expect do
            p.kick user2
        end.to change{p.managers.count}.by(-1)
    end

    it "should be possible to pass ownership" do
        u = user
        u2 = user2
        expect(user2.own_projects.count).to be(1)
        expect(user.own_projects.count).to be(1)
        p = create_project(owner: user)
        expect(user.own_projects.count).to be(2)
        expect do 
            p.pass_ownership_to user2
        end.to change{p.owner}.from(user).to(user2).and change{user.own_projects.count}.from(2).to(1).and change{user2.own_projects.count}.by(1)
    end

    it "should be possible to change user permissions" do
        p = create_project(owner: user)
        p.add_reader user2

        expect do
            p.change_permission user2, :write
        end.to change{p.readers.count}.by(-1).and change{p.writers.count}.by(1)
        expect do
            p.change_permission user2, :manage
        end.to change{p.writers.count}.by(-1).and change{p.managers.count}.by(1)
        p.change_permission user2, :read
        expect do
            p.change_permission user2, :read
        end.to_not change{p.readers.count}
    end

    it "should be shareable by link" do
        p = create_project(owner: user)
        expect(p.is_shared_with_link).to be(false)

        expect do 
            p.share_by_link
        end.to  change{p.is_shared_with_link}.from(false).to(true)

    end
    it "should then be possible to unshare link" do
        p = create_project(owner: user)
        p.share_by_link
        expect do 
            p.stop_sharing_by_link
        end.to  change{p.is_shared_with_link}.from(true).to(false)
    end

    it "should have #all_users method which would combine everybody who it is shared with" do
        p = create_project(owner: user) 
        n_readers = 25
        n_writers = 25
        n_managers = 10
        readers = FactoryGirl.create_list(:user, n_readers)
        writers = FactoryGirl.create_list(:user, n_writers)
        managers = FactoryGirl.create_list(:user, n_managers)
        p.add_readers readers
        p.add_writers writers
        p.add_managers managers
        expect(p.all_users.count).to be(n_readers + n_writers + n_managers + 1) # 1 for owner
        expect(p.all_users).to include(user)
    end

    it "shouldn't be possible to share with owner" do
        p = create_project(owner: user)
        expect{p.add_reader user}.to_not change{p.readers.count}
        expect{p.add_writer user}.to_not change{p.writers.count}
        expect{p.add_manager user}.to_not change{p.managers.count}
    end
end
