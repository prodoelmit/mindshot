require Rails.root.join("app/models/user_permission.rb")
class UserProject < ApplicationRecord
    include UserPermission

    belongs_to :project
    belongs_to :user

end
