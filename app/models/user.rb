class User < ApplicationRecord
  acts_as_token_authenticatable
  has_many :microposts
  has_many :publications

  # PROJECTS
  has_many :user_projects
  has_many :own_projects, class_name: "Project", foreign_key: :owner_id, inverse_of: 'owner'

  has_many :projects, through: :user_projects, class_name: "Project"

  has_many :user_projects_readable, -> {where level: 'r'}, class_name: "UserProject"
  has_many :user_projects_writeable, -> {where level: 'w'}, class_name: "UserProject"
  has_many :user_projects_manageable, -> {where level: 'm'}, class_name: "UserProject"

  has_many :readable_projects, through: :user_projects_readable, class_name: "Project", source: :project
  has_many :writeable_projects, through: :user_projects_writeable, class_name: "Project", source: :project
  has_many :manageable_projects, through: :user_projects_manageable, class_name: "Project", source: :project
  has_many :projects, through: :user_projects
  has_many :project_publications, through: :projects, class_name: "Publication", source: :publications

  after_create :create_default_project!

  #REPORTS
  has_many :user_reports
  has_many :own_reports, class_name: "Report", foreign_key: :owner_id, inverse_of: 'owner'

  has_many :reports, through: :user_reports, class_name: "Report"

  has_many :user_reports_readable, -> {where level: 'r'}, class_name: "UserReport"
  has_many :user_reports_writeable, -> {where level: 'w'}, class_name: "UserReport"
  has_many :user_reports_manageable, -> {where level: 'm'}, class_name: "UserReport"

  has_many :readable_reports, through: :user_reports_readable, class_name: "Report", source: :report
  has_many :writeable_reports, through: :user_reports_writeable, class_name: "Report", source: :report
  has_many :manageable_reports, through: :user_reports_manageable, class_name: "Report", source: :report
  has_many :reports, through: :user_reports
  has_many :report_publications, through: :reports, class_name: "Publication", source: :publications

  after_create :create_default_report!


  validates_presence_of :username
  validates_format_of :username, with: /\A[a-z\d](?:\-?[a-z\d]){0,38}\Z/i
  validates_uniqueness_of :username_lowercase
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def projects_with_permissions
      self.projects.select('*, projects.id as id, user_projects.level as level')
  end

  def reports_with_permissions
      self.reports.select('*, reports.id as id, user_reports.level as level')
  end

  def item_permission p
      if p.is_a? Project
          p_with_p = self.projects_with_permissions.find_by(id: p.id)
      elsif p.is_a? Report
          p_with_p = self.reports_with_permissions.find_by(id: p.id)
      end
      UserPermission::ClassMethods.level_to_permission p_with_p.level unless p_with_p.nil?
  end

  def can_read(p)
      (p.owner == self) or ( (item_permission p).in? %i[write manage read] )
  end

  def can_write(p)
      (p.owner == self) or ( (item_permission p).in? %i[write manage] )
  end

  def can_manage(p)
      (p.owner == self) or ( (item_permission p).in? %i[manage] )
  end

  def create_default_project!
      p = self.own_projects.create(name: "Default project of @#{self.username}")
      update(current_project_id: p.id)
  end

  def leave_project(project)
      self.projects.delete project
  end

  def create_default_report!
      p = self.own_reports.create(name: "Default report of @#{self.username}")
      update(current_report_id: p.id)
  end

  def leave_report(report)
      self.reports.delete report
  end

  def username= username
      super
      lowercase_username!
  end

  def current_project= p
      if can_write(p)
          update(current_project_id: p.id)
      end
  end

  def current_project
      Project.find_by(id: self.current_project_id)
  end

  def current_report= p
      if can_write(p)
          update(current_report_id: p.id)
      end
  end

  def current_report
      Report.find_by(id: self.current_report_id)
  end

  def all_projects
      own_projects + projects
  end

  def all_reports
      own_reports + reports
  end

  def stats_per_day_per_project(requesting_user)
      all = microposts.where(project_id: requesting_user.all_projects).order(:created_at).pluck(:created_at, :project_id)
      grouped_by_project = all.group_by{|arr| arr[1]}
      p grouped_by_project

      counted  = grouped_by_project.map do |k1, v1|
          dates = v1.map {|x| x[0].to_date}
          grouped_projects = dates.group_by {|x| x}
          counted_projects = grouped_projects.map {|k2, v2| [k2, v2.count]}

          [k1, counted_projects]

      end

      counted

  end



  private

  def lowercase_username!
      if self.username 
          self.username_lowercase = self.username.downcase
      end
  end


end
