class Project < ApplicationRecord 
    belongs_to :owner, class_name: "User"

    has_many :user_projects
    has_many :publications

    has_many :users, through: :user_projects, source: :user

    has_many :user_projects_readable, -> {where level: 'r'}, class_name: "UserProject"
    has_many :readers, class_name: "User", through: :user_projects_readable, source: :user

    has_many :user_projects_writeable, -> {where level: 'w'}, class_name: "UserProject"
    has_many :writers,  class_name: "User", through: :user_projects_writeable, source: :user

    has_many :user_projects_manageable, -> {where level: 'm'}, class_name: "UserProject"
    has_many :managers,  class_name: "User", through: :user_projects_manageable, source: :user

    has_many :microposts
    before_save :create_share_link
    before_destroy :destroy_shares

    validates_presence_of :owner
    validates_presence_of :name
    validates_uniqueness_of :share_link

    def add_reader(user)
        begin 
            self.users.delete user
            self.readers << user
        end  unless user == owner
    end

    def add_readers(users)
        begin 
            self.users.delete users
            self.readers << users.select{|user| user != owner}
        end 
    end

    def add_writer(user)
        begin 
            self.users.delete user
            self.writers << user 
        end unless user == owner
    end
    def add_writers(users)
        begin 
            self.users.delete users
            self.writers << users.select{|user| user != owner}
        end
    end

    def add_manager(user)
        begin 
            self.users.delete user
            self.managers << user 
        end unless user == owner
    end
    def add_managers(users)
        begin 
            self.users.delete users
            self.managers << users.select{|user| user != owner}
        end
    end

    def kick user
        self.users.delete user
    end

    def pass_ownership_to user
        prev_owner = self.owner
        self.owner = user
        save
        add_manager prev_owner
    end

    def change_permission user, permission
        u_p = user_projects.find_by(user_id: user.id)
        if u_p 
            u_p.set_permission permission
            u_p.save
        else
            case permission
            when :write
                add_writer user
            when :read 
                add_reader user
            when :manage
                add_manager user
            end
        end
    end

    def user_permission user
        u_p = user_projects.find_by(user_id: user.id) and u_p.permission
    end

    def create_share_link
        self.share_link = SecureRandom.urlsafe_base64(32)
    end

    def share_by_link
        self.is_shared_with_link = true
    end

    def stop_sharing_by_link
        self.is_shared_with_link = false
    end


    def all_users
        users.to_a << owner
    end


    def users_with_permissions
      self.users.select('*, users.id as id, user_projects.level as level').map
    end

    private
    def destroy_shares
        self.user_projects.delete_all
    end

end
