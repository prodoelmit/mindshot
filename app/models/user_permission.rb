module UserPermission

    def permission
        UserPermission::ClassMethods.level_to_permission self.level
    end

    def set_permission permission
        self.level = UserPermission::ClassMethods.permission_to_level permission
    end

    module ClassMethods
        def self.level_to_permission level
            case level
            when 'r'
                :read
            when 'w'
                :write
            when 'm'
                :manage
            end
        end

        def self.permission_to_level permission
            case permission
            when :read
                'r'
            when :write
                'w'
            when :manage
                'm'
            end
        end
    end



end
