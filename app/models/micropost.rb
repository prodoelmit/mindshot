require 'microposts_filter'
class Micropost < ApplicationRecord
  attr_accessor :content_archive
  before_destroy :clean_files, prepend: true
  belongs_to :project
  belongs_to :user

  has_and_belongs_to_many :reports
  validates_presence_of :user
  validates_presence_of :tex
  validates_presence_of :html
  validates_presence_of :content_path
  validates_presence_of :project

  scope :written_by, -> (user_id) {where user_id: user}
  scope :in_project, -> (project_id) {where project_id: project_id}

  def save_file(file)
    relative_filepath = Time.now.strftime('%Y-%m-%d_%H-%M-%S_%L')
    path = Rails.root.join("public","system","micropost_contents", relative_filepath)
    FileUtils.mkdir_p(path)

    system("tar -zxvf #{file.path} -C #{path} --strip-components=1 > /dev/null")
    self.content_path = relative_filepath


    return $?.exitstatus <= 0
  end

  def clean_files
    system "rm -R #{full_content_path(self.content_path)}"
  end

  def good_html
    h = vary_references(self.html)
    h.gsub!(":project_id", project.id.to_s)
    h.gsub!("data-href-dummy","href")
    h
  end


  def full_content_path(path = nil)
    if (path.nil?)
      Rails.root.join("public","system","micropost_contents",self.content_path)
    else
      Rails.root.join("public","system","micropost_contents",path)
    end
  end

  def self.filter(query, current_user)
    f = MicropostsFilter.new
    f.current_user = current_user
    f.query = query
    p f
    return f.microposts
  end



  def add_to_report(report)
      report.microposts << self
  end


  def remove_from_report(report)
      reports.delete report
  end

  private

  def vary_references(html)
    if (html)
      modhtml = html.gsub(/a class="ref" href="#([^"]+)" ref="([^"]+)"/, "a class=\"ref\" href=\"#\\1_#{hash}\" ref=\"\\1_#{hash}\"")
      modhtml = modhtml.gsub(/<span class="label" id="([^"]+)">/, "<span class=\"label\" id=\"\\1_#{hash}\">")
      return modhtml
    else 
      return ""
    end

  end


  def hash
    Digest::MD5.hexdigest(self.created_at.to_s)[0..6]
  end

end
