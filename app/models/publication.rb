class Publication < ApplicationRecord
    has_attached_file :pdf
    validates_attachment_content_type :pdf, content_type: %r"application/pdf.*"
    belongs_to :user
    belongs_to :project

    validates_presence_of :user
    validates_presence_of :project
    validates_presence_of :bibtex
    validates_presence_of :author
    validates_presence_of :title
    validates_presence_of :ref
    validates_uniqueness_of :ref, scope: :project_id




    def parseBibTeX!
        bt = BibTeX.parse self.bibtex
        b = bt.first
        self.author = b.author
        self.title = b.title
        self.ref = b.key

        if bt.errors.empty?
            return true
        else 
            self.errors.add(:base, bt.errors)
            return false
        end
    end

    def copy_to_project project
      pub = self.dup
      pub.project_id = project.id
      pub.save
      return pub
    end

    def self.filter(query, current_user)
      f = PublicationsFilter.new
      f.current_user = current_user
      f.query = query
      return f.publications
    end

end
