require Rails.root.join("app/models/user_permission.rb")
class UserReport < ApplicationRecord
    include UserPermission

    belongs_to :report
    belongs_to :user
end
