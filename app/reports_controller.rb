require Rails.root.join('app/models/user_report.rb')
class ReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_report, except: %i[index new create share_with_user]

  def set_as_current
    (render_404 and return) unless @can_write

    current_user.current_report = @report

    render json: {status: "success"}
  end
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_report
    @report = report.find_by(id: params[:id])
    render_404 if @report.nil?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def report_params
    params.fetch(:report, {})
  end
end
