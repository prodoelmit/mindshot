class PublicationsFilter < AbstractFilter

  def publications
    Publication.where(get_hash).order(created_at: :desc)
  end



end
