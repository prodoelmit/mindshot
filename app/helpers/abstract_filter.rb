class AbstractFilter
  attr_accessor :user_ids, :project_ids, :query, :current_user

  def query= query
    @query = query
    parse_query @query
    finalize_query
  end

  def query
    return @query
  end

  def parse_query q
    queryBits = q.scan(/([a-z](?:-?[a-z\d])+:(?:\s*(?:(?:#?[0-9\-]+)|me|my|(?:@?(?:[a-z\d](?:\-?[a-z\d]){0,38}))),?)+)/i)
    if queryBits.is_a? Array 
      queryBits.each do |b|
        parse_bit b.first
      end
    end
  end

  def parse_bit bit
    key, value = bit.split(':', 2)
    case key 
    when /(use?rs?|u)/
      parse_users value
    when /(projects?|prj|p)/
      parse_projects value
    when /(date-?from|df)/
      parse_date value, :from
    when /(date-?to|dt)/
      parse_date value, :to
    end
  end

  def parse_users s
    s.gsub!(/\bme\b/i, current_user.username.to_s)
    p s
    usernames = s.scan /([a-z\d](?:\-?[a-z\d]){0,38})/i
    p s
    p usernames
    usernames.flatten!
    if usernames.is_a?(Array)
      usernames = usernames.map{|name| name.downcase}
    else
      usernames = usernames.downcase
    end

    @user_ids = User.where(username_lowercase: usernames).ids
  end

  def parse_projects s
    if s.include? 'my'
      my_projects = current_user.own_projects
      ss = my_projects.map { |p| p.id.to_s }
      joined = ss.join(',')
      s.gsub!(/\bmy\b/i, joined)
    end
    ids = s.scan(/(?:#?([0-9]+))/) 
    @project_ids = (ids.is_a? Array) ? ids.flatten : ids
  end

  def parse_date s, param
    d = Date.parse s
    case param
    when :from
      @date_from = d
    when :to
      @date_to = d
    end
  end

  def finalize_query
    Rails.logger.debug "ONE"
    unless @date_from.nil? && @date_to.nil?
      @date_from ||= Date.new(1970,1,1)
      @date_to ||= Date.today
    end

    Rails.logger.debug "TWO"

    readable_project_ids = current_user.all_projects.map { |p| p.id.to_s }
    Rails.logger.debug "THREE"
    if @project_ids.nil?
      Rails.logger.debug "FOUR"
      @project_ids = readable_project_ids
    else
      Rails.logger.debug "FIVE"
      @project_ids &= readable_project_ids
    end
  end

  def get_hash
    hash = {}
    hash[:user_id] = @user_ids unless @user_ids.nil?
    hash[:project_id] = @project_ids
    hash[:created_at] = @date_from..@date_to unless @date_from.nil?
    hash

  end

end
