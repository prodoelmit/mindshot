class MicropostsFilter < AbstractFilter

  def microposts
    Micropost.where(get_hash).order(created_at: :desc)
  end

end
