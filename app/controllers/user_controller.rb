class UserController < ApplicationController
    before_action :authenticate_user!

    def settings
    end

    def stats
      @user = User.find_by username: params[:username]
      respond_to do |format|
        format.html {}
        format.json { render json:  @user.stats_per_day_per_project(current_user) }
      end
    end

    def update_password
      @user = User.find(current_user.id)
      if @user.update(user_params)
        bypass_sign_in(@user)
        redirect_to root_path
      else
        redirect_to 'user#settings'
      end
    end

    def show 
      @user = User.find_by(username: params[:username]) or (render_404 and return)
      projects_readable_by_current_user = current_user.all_projects.map{|p| p.id}
      @own_projects = @user.own_projects.where(id: projects_readable_by_current_user)
      @projects = @user.projects.where(id: projects_readable_by_current_user)
    end

    def microposts
      @user = User.find_by username: params[:username]
      redirect_to microposts_path(filter: "user:#{@user.username}")
    end

    private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end
end

