class PublicationsController < ApplicationController
    before_action :set_publication, only: [:show, :edit, :update, :destroy, :pdf]
    before_action :authenticate_user!

    # GET /publications
    # GET /publications.json
    def index
      @query = params[:filter]
      if params[:filter]
        @publications = Publication.filter params[:filter], current_user
      else
        @publications = Publication.filter "", current_user
      end
    end

    # GET /publications/1
    # GET /publications/1.json
    def show
        if @publication.nil?
            render :missing
        else
            render :show
        end
    end


    # GET /publications/new
    def new
        @publication = Publication.new
    end

    # GET /publications/1/edit
    def edit
      (render_404 and return) unless current_user.can_write @publication.project
      
    end

    # POST /publications
    # POST /publications.json
    def create
        @publication = Publication.new(publication_params)
        @publication.user = current_user

        parsed = false
        if @publication.parseBibTeX!
            parsed = true
        end

        respond_to do |format|
            if parsed and @publication.save 
                format.html { redirect_to @publication, notice: 'Publication was successfully created.' }
                format.json { render :show, status: :created, location: @publication }
            else
                format.html { render :new }
                format.json { render json: @publication.errors, status: :unprocessable_entity }
            end
        end
    end

    # PATCH/PUT /publications/1
    # PATCH/PUT /publications/1.json
    def update
        respond_to do |format|
            parsed = false
            if @publication.parseBibTeX!
                parsed = true
            end
            if parsed and @publication.update(publication_params)
                format.html { redirect_to @publication, notice: 'Publication was successfully updated.' }
                format.json { render :show, status: :ok, location: @publication }
            else
                format.html { render :edit }
                format.json { render json: @publication.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /publications/1
    # DELETE /publications/1.json
    def destroy
        @publication.destroy
        respond_to do |format|
            format.html { redirect_to publications_url, notice: 'Publication was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    def pdf
        @title = @publication.ref
        send_data File.open(@publication.pdf.path).read, disposition: 'inline', filename: "#{@publication.ref}.pdf", type: "application/pdf"
    end

    def getIds
        ps = params.require(:refs)
        ids = ps.map do |project, ref| 
            p = project.publications.find_by ref: ref
            if p.nil?
                -1
            else
                p.id
            end
        end


        render json: ids
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_publication
        if params[:project_id]
            @p = Project.find_by id: params[:project_id] 
            return unless current_user.can_read(@p)
            return if @p.nil?
            if (/^[0-9]+$/ =~ params[:id]).nil? 
              @publication = @p.publications.find_by ref: params[:id]
            else
              @publication = @p.publications.find_by id: params[:id] 
            end
        else 
          @publication = Publication.find_by id: params[:id]
          return if @publication.nil?
          @publication = nil if !(current_user.can_read @publication.project)
        end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def publication_params
      params.fetch(:publication, {}).permit([:bibtex, :pdf, :project_id])
    end

end
