class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    acts_as_token_authentication_handler_for User
  protect_from_forgery with: :exception

  protected
  def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end
    def render_404
            render file: "#{Rails.root}/public/404.html", status: :not_found, layout: false
            return true
    end
    def render_403
            render file: "#{Rails.root}/public/403.html", status: :forbidden, layout: false
            return true
    end
end
