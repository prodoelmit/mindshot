require Rails.root.join("lib/parser/parser.rb")
class MicropostsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_micropost, only: [:show, :edit, :update, :destroy, :tex, :add_to_current_report, :remove_from_current_report]
    skip_before_action :verify_authenticity_token

    # GET /microposts
    # GET /microposts.json
    def index
      @query = params[:filter]
      if params[:filter]
        @microposts = Micropost.filter params[:filter], current_user
      else
        @microposts = Micropost.filter "", current_user
      end
    end

    # GET /microposts/1
    # GET /microposts/1.json
    def show
      p current_user.can_read @micropost.project
      (render_404 and return) unless current_user.can_read @micropost.project
    end


    # GET /microposts/1/edit
    def edit
        (render_404 and return) unless current_user == @micropost.user
    end

    # POST /microposts
    # POST /microposts.json
    def create
        @micropost = current_user.current_project.microposts.new(micropost_params)
        @micropost.user = current_user
        p @micropost
        file_saved = @micropost.save_file(micropost_file)
        
        tex = micropost_params[:tex]
        th = TexConverter::TexHandler.new
        th.relative_path = "/system/micropost_contents/#{@micropost.content_path}"
        th.user_id = current_user.id
        parseResult = th.texToHtml tex 

        s = parseResult[:status]
        if s.empty?
            html = parseResult[:tree]
            @micropost.html = html
        end


        respond_to do |format|
            if file_saved and @micropost.save
                format.html { redirect_to @micropost, notice: 'Micropost was successfully created.' }
                format.json { render :show, status: :created, location: @micropost }
            else
                @micropost.clean_files
                format.html { render :new }
                format.json { render json: @micropost.errors, status: :unprocessable_entity }
            end
        end
    end

    # PATCH/PUT /microposts/1
    # PATCH/PUT /microposts/1.json
    def update
        (render_404 and return) unless @micropost.user == current_user
        respond_to do |format|
            if @micropost.update(micropost_params)
                format.html { redirect_to @micropost, notice: 'Micropost was successfully updated.' }
                format.json { render :show, status: :ok, location: @micropost }
            else
                format.html { render :edit }
                format.json { render json: @micropost.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /microposts/1
    # DELETE /microposts/1.json
    def destroy
        @micropost.destroy
        respond_to do |format|
            format.html { redirect_to microposts_url, notice: 'Micropost was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    def tex
        render plain: @micropost.tex, layout: false
    end

    def add_to_current_report
        (render_404 and return) unless current_user.can_read @micropost.project
        @micropost.add_to_report current_user.current_report

        render json: {status: "success"}
    end
    def remove_from_current_report
        (render_404 and return) unless current_user.can_read @micropost.project

        @micropost.remove_from_report current_user.current_report

        render json: {status: "success"}
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_micropost
        @micropost = Micropost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def micropost_params
        params.fetch(:micropost, {}).permit(:commit, :tex, :project_id)
    end

    def micropost_file
        params.fetch(:micropost, {}).permit(:content_archive)[:content_archive]
    end



end
