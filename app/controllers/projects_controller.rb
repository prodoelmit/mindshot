require Rails.root.join('app/models/user_project.rb')
class ProjectsController < ApplicationController 
  before_action :authenticate_user!
  before_action :set_project, except: %i[index new create share_with_user]
  before_action :set_permissions, except: %i[index new create share_with_user]

  # GET /projects
  # GET /projects.json
  def index
    @own_projects = current_user.own_projects
    @projects = current_user.projects_with_permissions
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    (render_404 and return) unless @can_read
  end

  # GET /projects/new
  def new
    @project = Project.new
    @project.owner_id = current_user.id
  end

  # GET /projects/1/edit
  def edit;
    (render_404 and return) unless @can_manage
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params.permit(%i[owner_id name description git_url]) )

    respond_to do |format|
      if @project.save
        format.html do
          redirect_to @project,
            notice: 'Project was successfully created.'
        end
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    (render_404 and return) unless @can_manage
    respond_to do |format|
      if @project.update(project_params.permit(%i[name git_url description]) )
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def leave
    unless @can_leave
      redirect_to project_path(@project), flash: { error: "You can't leave your own project. You may delete it or pass ownership to somebody else." }
    end

    current_user.leave_project(@project)
    redirect_to projects_path, notice: "You've left project #{@project.name}"
  end

  def sharesettings
    unless @can_manage
      redirect_to project_path(@project), flash: { error: "You don't have rights to manage this project" }
    end
  end

  def share_with_user
    prms = params.permit([:username, :id, :async])
    @project = Project.find_by(id: prms[:id])
    set_permissions
    (render_404 and return) unless @can_manage

    username = prms[:username]
    @user = User.find_by(username: username)
    errors = []
    status = :success
    user = nil

    if @project.nil?
      errors << "No project found with id #{prms[:id]}"
      status = :fail
    elsif @user.nil?
      errors << "No user found with username #{username}"
      status = :fail
    elsif @user.in? @project.users
      errors << "User #{username} already has permissions in this project"
      status = :fail
    elsif @user == @project.owner
      errors << "User #{username} is owner of this project"
      status = :fail
    else
      if @project.add_reader @user 
        status = :success
        user = @user
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if prms[:async]
      render json: {status: status, errors: errors, user: user.to_json}
    else 
      redirect_to project_sharesettings_path(@project)
    end
  end
  
  def set_user_permission 
    (render_404 and return) unless @can_manage
    prms = params.permit([:username, :level, :async])

    user = User.find_by(username: prms[:username])
    level = prms[:level]
    async = prms[:async]

    errors = []
    status = :success
    permission = UserProject::ClassMethods.level_to_permission level

    if user.nil?
      status = :fail
      errors << "No user found with id #{prms[:username]}"
    elsif !(user.in? @project.users)
      status = :fail
      errors << "User #{user.username} isn't participating in this project"
    else
      if @project.change_permission user, permission
        status = :success
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if async
      render json: {status: status, errors: errors, level: level}
    else
      #redirect_to project_sharesettings_path(@project)
      redirect_to project_sharesettings_path(@project)
    end

  end

  def kick_user
    (render_404 and return) unless @can_manage
    prms = params.permit([:username, :async])
    user = User.find_by username: prms[:username]
    errors = []
    status = :success

    if user.nil?
      status = :fail
      errors << "No user found with id #{prms[:username]}"
    elsif user == @project.owner
      status = :fail
      errors << "Can't kick owner"
    elsif !(user.in? @project.users)
      status = :fail
      errors << "User with id #{prms[:username]} isn't participant of this project"
    else
      if @project.kick user
        status = :success
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if prms[:async]
      render json: {status: status, errors: errors}
    else
      redirect_to project_sharesettings_path(@project)
    end


  end

  def set_as_current
    (render_404 and return) unless @can_write

    current_user.current_project = @project

    render json: {status: "success"}
  end

  def microposts
    redirect_to microposts_path(filter: "project:##{@project.id}")
  end

  def publications
    redirect_to publications_path(filter: "project:##{@project.id}")
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find_by(id: params[:id])
    render_404 if @project.nil?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def project_params
    params.fetch(:project, {})
  end

  def set_permissions
    @is_owner = @project.owner == current_user
    @can_leave = !@is_owner
    if @is_owner
      @can_manage = true
      @can_write = true
      @can_read = true
    else
      perm = @project.user_permission(current_user)
      unless perm.nil?
        @can_read = perm.in? %i[read write manage]
        @can_write = perm.in? %i[write manage]
        @can_manage = perm.in? [:manage]
      end
    end
  end
end
