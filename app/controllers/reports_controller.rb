require Rails.root.join('app/models/user_report.rb')
class ReportsController < ApplicationController 
  before_action :authenticate_user!
  before_action :set_report, except: %i[index new create share_with_user]
  before_action :set_permissions, except: %i[index new create share_with_user]

  # GET /reports
  # GET /reports.json
  def index
    @own_reports = current_user.own_reports
    @reports = current_user.reports_with_permissions
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
    (render_404 and return) unless @can_read
  end

  # GET /reports/new
  def new
    @report = Report.new
    @report.owner_id = current_user.id
  end

  # GET /reports/1/edit
  def edit;
    (render_404 and return) unless @can_manage
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params.permit(%i[owner_id name description git_url]) )

    respond_to do |format|
      if @report.save
        format.html do
          redirect_to @report,
            notice: 'Report was successfully created.'
        end
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    (render_404 and return) unless @can_manage
    respond_to do |format|
      if @report.update(report_params.permit(%i[name git_url description]) )
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def leave
    unless @can_leave
      redirect_to report_path(@report), flash: { error: "You can't leave your own report. You may delete it or pass ownership to somebody else." }
    end

    current_user.leave_report(@report)
    redirect_to reports_path, notice: "You've left report #{@report.name}"
  end

  def sharesettings
    unless @can_manage
      redirect_to report_path(@report), flash: { error: "You don't have rights to manage this report" }
    end
  end

  def share_with_user
    prms = params.permit([:username, :id, :async])
    @report = Report.find_by(id: prms[:id])
    set_permissions
    (render_404 and return) unless @can_manage

    username = prms[:username]
    @user = User.find_by(username: username)
    errors = []
    status = :success
    user = nil

    if @report.nil?
      errors << "No report found with id #{prms[:id]}"
      status = :fail
    elsif @user.nil?
      errors << "No user found with username #{username}"
      status = :fail
    elsif @user.in? @report.users
      errors << "User #{username} already has permissions in this report"
      status = :fail
    elsif @user == @report.owner
      errors << "User #{username} is owner of this report"
      status = :fail
    else
      if @report.add_reader @user 
        status = :success
        user = @user
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if prms[:async]
      render json: {status: status, errors: errors, user: user.to_json}
    else 
      redirect_to report_sharesettings_path(@report)
    end
  end
  
  def set_user_permission 
    (render_404 and return) unless @can_manage
    prms = params.permit([:username, :level, :async, :id])

    user = User.find_by(username: prms[:username])
    level = prms[:level]
    async = prms[:async]

    errors = []
    status = :success
    permission = UserReport::ClassMethods.level_to_permission level

    if user.nil?
      status = :fail
      errors << "No user found with id #{prms[:username]}"
    elsif !(user.in? @report.users)
      status = :fail
      errors << "User #{user.username} isn't participating in this report"
    else
      if @report.change_permission user, permission
        status = :success
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if async
      render json: {status: status, errors: errors, level: level}
    else
      #redirect_to report_sharesettings_path(@report)
      redirect_to report_sharesettings_path(@report)
    end

  end

  def kick_user
    (render_404 and return) unless @can_manage
    prms = params.permit([:username, :async])
    user = User.find_by username: prms[:username]
    errors = []
    status = :success

    if user.nil?
      status = :fail
      errors << "No user found with id #{prms[:username]}"
    elsif user == @report.owner
      status = :fail
      errors << "Can't kick owner"
    elsif !(user.in? @report.users)
      status = :fail
      errors << "User with id #{prms[:username]} isn't participant of this report"
    else
      if @report.kick user
        status = :success
      else
        status = :fail
        errors << "Unknown error"
      end
    end

    if prms[:async]
      render json: {status: status, errors: errors}
    else
      redirect_to report_sharesettings_path(@report)
    end


  end

  def set_as_current
    (render_404 and return) unless @can_write

    current_user.current_report = @report

    render json: {status: "success"}
  end

  def microposts
    redirect_to microposts_path(filter: "report:##{@report.id}")
  end

  def publications
    redirect_to publications_path(filter: "report:##{@report.id}")
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_report
    @report = Report.find_by(id: params[:id])
    render_404 if @report.nil?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def report_params
    params.fetch(:report, {})
  end

  def set_permissions
    @is_owner = @report.owner == current_user
    @can_leave = !@is_owner
    if @is_owner
      @can_manage = true
      @can_write = true
      @can_read = true
    else
      perm = @report.user_permission(current_user)
      unless perm.nil?
        @can_read = perm.in? %i[read write manage]
        @can_write = perm.in? %i[write manage]
        @can_manage = perm.in? [:manage]
      end
    end
  end
end
