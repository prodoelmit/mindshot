# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
#
addUserShare = (u) ->
  console.log u
  new_div = $("#example-share-user-row").clone(true).first()
  $(new_div).removeAttr('id')
  $(new_div).find("div#username").text(u.username)
  $(new_div).find("input#username").val(u.username)
  $(new_div).find(".share-user-username").text(u["username"])
  $(new_div).find(".share-permission").attr("data-username", u["username"])
  $("#share-user-list").append(new_div)

getUsername = (t) ->
  return $(t).closest(".share-user").find("#username").text()



revert_permission = (e) ->
  console.log e
  $(e).val($(e).attr('data-level'))

  



$(document).ready ->
  if $(".projects#sharesettings").length == 0 && $(".projects#index").length == 0 && $(".projects#show").length == 0
      return

  $("#share-user-add-user").submit (e) ->
    form = e.target
    href = form.action
    field = $(form).find("#username")
    username =  $(field).val()
    data = {username: username, async: true}
    console.log(data)
    $.post href, data
      .done (r) ->
        console.log(r)
        status = r.status
        if status == "fail"
          console.log "fail", r.errors
        else if status == "success"
          console.log "success"
          console.log r.user
          addUserShare JSON.parse(r.user)

      .fail ->
        console.log("fail")

    e.preventDefault()

  $(".share-permission").change (e) ->
    t = e.target
    username = $(t).attr('data-username')
    username = getUsername(t)
    project_id = $("#project_id").text()
    level = this.value
    data = {username: username, level: level, async: true}
    $.post "/projects/#{project_id}/set_user_permission/", data
      .done (r) ->
        console.log(r)
        status = r.status
        if status == "fail"
          warn "Couldn't change user permission: " + r.errors
        else if status == "success"
          $(t).attr('data-level', r.level)
          $(t).val(r.level)
      .fail ->
        warn "Couldn't change user permission"
        revert_permission t

  $("a.project-kick-user").click (e)->
    t = e.target
    e.preventDefault
    username = getUsername(t)
    href = $(t).attr('href')
    console.log username
    data = {username: username, async: true}
    $.post href, data
      .done (r) ->
        if r.status == "fail"
          warn "Couldn't kick user: " + r.errors
        else if r.status == "success"
          t.closest(".share-user").remove()
      .fail (r) ->
        warn "Couldn't kick user"
    return false

  $("a.project-is-current-indicator").click (e)  ->
    e.preventDefault
    t = e.target
    return if $(t).hasClass("is-current")
    console.log(t)
    href = $(t).attr('href')
    console.log(href)
    $.post href
      .done (r) ->
        if r["status"] == "success"
          $(".is-current").removeClass("is-current btn-success disabled").addClass("is-not-current btn-default")
            .text("Make current")
          $(t).removeClass("is-not-current btn-default").addClass("is-current btn-success disabled")
            .text("Current")
        else
          console.log(r)
      .fail (r) ->
        console.log(r)
    return false






