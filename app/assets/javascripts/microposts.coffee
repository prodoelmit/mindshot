# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
resolveCites = ->
    cites = $('.cite')
    labels = $('.label')
    labelsMap = {}
    $.each labels, (index, label) ->
        labelName = label.id
        labelsMap[labelName] = label
        return
    citesWithNumbers = {}
    i = 0
    citeRefs = (cite.getAttribute('cite') for cite in cites)
    $.post "/misc/getPublicationIds", {'refs[]': citeRefs}, (data) ->
        console.log(data)
        for i in [0...cites.length]
            cite = cites[i]
            cite.text = (if (data[i] == -1) then "?" else data[i])


    console.log(citeRefs)



$(document).ready ->
  if $(".microposts#index").length == 0 && $(".microposts#show").length == 0
    return
  resolveCites() unless $(".cite").empty()
