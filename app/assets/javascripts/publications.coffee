


$(document).ready -> 
  if $(".publications#edit").length == 0 && $(".publications#index").length == 0
    return

  $('.clipboard-btn').tooltip {
      trigger: 'click',
      placement: 'bottom'
  }

  setTooltip = (btn, message) ->
      $(btn).tooltip 'hide'
          .attr('data-original-title', message)
          .tooltip('show')
  hideTooltip = (btn) ->
      setTimeout ->
          $(btn).tooltip 'hide'
      , 1000

  clipboard = new Clipboard('.clipboard-btn')
  clipboard.on 'success', (e) -> 
      setTooltip e.trigger, 'Copied!'
      hideTooltip e.trigger
      console.log 'Copied'

  clipboard.on 'error', (e) -> 
      setTooltip e.trigger, 'Failed!'
      hideTooltip e.trigger
      console.log 'Failed'





