$(document).ready ->

  if $(".users#stats").length == 0
    return
  else
    console.log "hi"

    username = $("#chart").attr("data-username")
    $.getJSON("/users/#{username}/stats.json")
      .done (r) ->
        traces = []
        for data in r
          xs = []
          ys = []
          p = data[0]
          v = data[1]
          for d in v
            xs.push new Date(d[0])
            ys.push d[1]
          trace = {
            x: xs,
            y: ys,
            name: p,
            type: 'bar',
            opacity: 0.5,
          }
          traces.push trace
        layout = {
          barmode: "stack",
          xaxis: {
            title: 'date',
            type: 'date'
          }
          yaxis: {
            title: "microposts/day",
            fixedrange: true
          }


        }
        Plotly.newPlot("chart", traces, layout)






