#!/usr/bin/ruby
require 'treetop'
require_relative 'equation_generator.rb'
require 'sqlite3'
require 'erb'
require 'pathname'
require 'securerandom'
require 'tempfile'
# require_relative 'sexp_parser.rb'
#require_relative 'sexp_parser.rb'

module TexConverter

$base_path = File.expand_path(File.dirname(__FILE__))

require File.join($base_path, 'node_extensions.rb')

$base_path = File.expand_path(File.dirname(__FILE__))

class Counter
    @@counters = {}
    @@pairCounters = {}

    def self.getCounter(name)
        counter = 0
        if @@counters[name].nil?
            @@counters[name] = 1
        else
            @@counters[name] += 1
        end
        counter = @@counters[name]
        return counter
    end

    def self.pairCounter(name)
        if @@pairCounters[name].nil?
            counter = getCounter(name)
            @@pairCounters[name] = counter
            return counter
        else
            counter = @@pairCounters[name]
            @@pairCounters.delete name
            return counter
        end
    end



end

class RefIndex
    @@db = SQLite3::Database.new "tmp/#{SecureRandom.urlsafe_base64}.db"

    @@db.execute <<-SQL
        create table pages (
            name varchar(50)
        )
    SQL

    @@db.execute <<-SQL
        create table labels (
            label varchar(50),
            pageId int
            )
    SQL

    def self.registerPage name
        TexHandler.compileLogWrite %-Registering page #{name}-
        @@db.execute("
            INSERT INTO pages (name) VALUES (?)", name)
        id = @@db.execute("SELECT last_insert_rowid()")[0][0]

        return id
    end

    def self.registerLabel label, pageId

        TexHandler.compileLogWrite %-Registering label #{label} in page with id #{pageId}-
        @@db.execute("INSERT INTO labels (label, pageId) VALUES (?, ?)", [label, pageId])
    end

    def self.resolveRef label
        rows =  @@db.execute("SELECT * FROM labels WHERE label = ?",label)
        row = rows[0]
        if row.nil?
            TexHandler.compileLogWrite %-Error resolving reference: #{label}-
            warn "Error resolving reference: #{label}"
            return nil
        else
            pageId = row[1]
            return pageId
        end
    end

    def self.getPageById id
        rows = @@db.execute("SELECT * FROM pages WHERE rowid = ?", id)
        row = rows[0]
        name = row[0]
        return name
    end

    def self.getPageByLabel label
        id = resolveRef label
        if id.nil?
            return "javascript:;"
        else
            return getPageById(id)
        end
    end


end

class Parser


    Treetop.load(File.join($base_path, 'sexp_parser.treetop'))
    @@parser = SexpParser.new


    # @return [Hash]
    # @param status [String ]
    def self.parseString(string)

      status = []
      tree = @@parser.parse(string)
      if (tree.nil?)
        status.push %-Parser:: Treetop got : #{@@parser.failure_reason} on line #{@@parser.failure_line}, column #{@@parser.failure_column}-
        puts @@parser.failure_reason
        puts @@parser.failure_line
        puts @@parser.failure_column
      else
      end

      return {tree: tree, status: status}
    end

    def self.parse(filename,htmlFilename)
        TexHandler.compileLogWrite %-Parser:: [START] Parsing file #{filename}.\n HtmlFilename is set to "#{htmlFilename}"-
        setCurrentPage (RefIndex.registerPage htmlFilename)
        data = File.read(filename)
        TexHandler.compileLogWrite "Parser:: Read data from #{filename}: #{data.length}"


        tree = @@parser.parse(data)
        TexHandler.compileLogWrite "Parser:: Treetop parser complete."


        if (tree.nil?)
            TexHandler.compileLogWrite "Parser:: Treetop got errors: #{@@parser.failure_reason} on line #{@@parser.failure_line}, column #{@@parser.failure_column}"
            puts @@parser.failure_reason
            puts @@parser.failure_line
            puts @@parser.failure_column
            #raise Exception, "Parse error at offset: #{@@parser.index}"
        else
            TexHandler.compileLogWrite "Parser:: Treetop parsing was succesful"
        end

        TexHandler.compileLogWrite %-[END] Parsing file #{filename}-
        return tree
    end



    def self.setCurrentPage pageId
        @@curPageId = pageId
    end

    def self.currentPage
        return @@curPageId
    end


end

class TexHandler
    @@gitLog = ""
    @@compileLog = ""


    attr_accessor :relative_path
    attr_accessor :user_id

    
    def initialize
        @relative_path = "/"
        @user_id = 0
    end

    def self.gitLogAdd str
        @@gitLog << Time.now.getutc.to_s << " " << str << "\n"
    end

    def self.gitLog
        @@gitLog
    end
    def self.compileLog
        @@compileLog
    end

    def self.compileLogWrite str
        @@compileLog << "#{Time.now.getutc.to_s} #{str}\n"
        Rails.logger.debug ("#{Time.now.getutc.to_s} #{str}\n")
    end

    def self.texFolder
        return @@texFolder
    end

    def self.preamblePath
        return @@preamblePath
    end


    def texToHtml(texString)
      parseResult = TexConverter::Parser.parseString(texString)
      html = ""
      if parseResult[:status].empty?
        Dir.mktmpdir do |tmpdir|
            TexConverter::TexCompiler.setPreamble(Rails.configuration.x.preamble)
            TexConverter::TexCompiler.setTmpFolder tmpdir
            TexConverter::TexCompiler.setOutFolder Rails.configuration.x.formulasPath
            TexConverter::TexCompiler.setDisplayOutFolder Rails.configuration.x.formulasDisplayPath

            html = parseResult[:tree].to_html(false, relative_path: @relative_path, user_id: @user_id)
            debughere = 0
          end
     else
         Rails.logger.debug parseResult[:status]
         
          html = ""
     end

      return {tree: html, status: parseResult[:status]}
    end

    # TexConverter::TexHandler.compileFolder(Rails.configuration.x.gitdir.join(name).to_s,
    #                                        Rails.configuration.x.compileddir.join(name),
    #                                        Rails.configuration.x.preamble,
    #                                        Rails.configuration.x.parserassetsdir
    def self.compileFolder(folderPath, outFolderPath, preamblePath, assetsPath)
        compileLogWrite %-[START] \t Compiling folder #{folderPath}\n
            \t\tOutput folder is set to #{outFolderPath}\n
            \t\tPreamblePath: #{preamblePath}\n
            \t\tAssetsPath: #{assetsPath}
        -
        @@texFolder = "#{outFolderPath}/formulas/"
        outFolder = Pathname.new(outFolderPath)
        outFolder.mkpath

        sourceFolder = Pathname.new(folderPath)
        texFolder = sourceFolder.join("sections")
        formulasFolder = outFolder.join("formulas")
        TexCompiler.setOutFolder("#{outFolderPath}/formulas/")

        TexCompiler.setDisplayOutFolder("formulas/")
        TexCompiler.setPreamble(preamblePath)

        Dir.mktmpdir do |tmpdir|
            tmpFolder = Pathname.new(tmpdir)
            compileLogWrite "Created temp dir #{tmpdir}"
            TexCompiler.setTmpFolder(tmpFolder.to_s)
            files = Dir[texFolder.join("*.tex"), texFolder.join("**/*.tex")]

            files.each do |f|
                compileLogWrite %-[START] processing file #{f}-
                name = Pathname.new(f).basename(".*").to_s
                compileLogWrite %-Basename = #{name}-
                begin
                    tree = Parser.parse(f,"#{name}.html")
                    erb = tree.to_html
                rescue StandardError => erbErrMsg
                    compileLogWrite %-Error converting parsetree of #{f} to html: #{erbErrMsg}-
                else
                    erbPathname = tmpFolder.join("#{name}.erb")
                    begin
                        erbFile = erbPathname.open("w")
                    rescue StandardError => errMsg
                        compileLogWrite %-Error opening file #{erbPathname}: #{errMsg}-
                    else
                        begin
                            erbFile.write(erb)
                        rescue StandardError => errMsg2
                            compileLogWrite %-Error writing to file #{erbPathname}: #{errMsg2}-
                        ensure
                            erbFile.close
                        end
                    end
                end

                compileLogWrite %-[END] processing file #{f}-
            end

            erbFiles = Dir[tmpFolder.join("*.erb")]


            erbFiles.each do |f|
                compileLogWrite %-[START] processing file #{f}-
                name = Pathname.new(f).basename(".*").to_s
                compileLogWrite %-Basename = #{name}-
                erb = File.read(f)
                htmlPathname = outFolder.join("#{name}.html")
                begin
                    htmlFile = htmlPathname.open("w")
                rescue StandardError => errMsg
                    compileLogWrite %- Error opening file #{htmlFile}: #{errMsg}-
                else
                    begin
                        htmlFile.write(ERB.new(erb).result)
                    rescue StandardError => errMsg
                        compileLogWrite %@Error writing erb->html result to file #{htmlPathname}: #{errMsg}@
                    ensure
                        htmlFile.close
                    end
                end
                compileLogWrite %-[END] processing file #{f}-
            end

        end

        begin
        FileUtils.cp_r(sourceFolder.join("figures"), outFolder)
        FileUtils.cp_r(Dir[assetsPath.join("*")], outFolder)
        FileUtils.rm(Dir[formulasFolder.join("*.tex")])
        FileUtils.rm(Dir[formulasFolder.join("*.aux")])
        FileUtils.rm(Dir[formulasFolder.join("*.pdf")])
        FileUtils.rm(Dir[formulasFolder.join("*.log")])
        rescue StandardError => errMsg
            compileLogWrite %-Error processing assets: #{errMsg}-
        end







    end

end


end
