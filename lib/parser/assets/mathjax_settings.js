window.MathJax = {
    TeX: {
        Macros: {
            vv: ["{\\vec{#1}}", 1],
            vi: "{\\vec{i}}",
            vj: "{\\vec{j}}",
            hvecT: ["{(#1,#2)^{\\mathstrut\\scriptscriptstyle{T}}}",2],
            m: "{\\text{м}}",
            dm: "{\\text{дм}}",
            km: "{\\text{км}}",
            cm: "{\\text{см}}",
            kg: "{\\text{кг}}",
            hour: "{\\text{ч}}",
            minute: "{\\text{мин}}"

        }
    }
}
