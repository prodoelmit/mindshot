
$(function(){
 var cImgs = document.getElementsByClassName("cImg");
 for (i = 0; i < cImgs.length; ++i) {
     var cImg = cImgs[i];
     var imgs = cImg.getElementsByTagName("img");
     for (j = 0; j < imgs.length; ++j) {
         img = imgs[j];
        var cwidth = img.getAttribute("cwidth");
        var width = parseFloat(cwidth) * 100;
        var widthString = width.toString() + "%";
        img.setAttribute("width", widthString);
     }
}

resolveRefs();

$(".ref").click( function() {
    var refName = $(this).attr("ref");
    var element = getElementByLabel(refName).parentNode;
    $('html,body').animate({ scrollTop: $(element).offset().top - ( $(window).height() - $(element).outerHeight(true) ) / 2 }, 200);
    

    return false;
});



}
);

function resolveRefs() {
    var refs = $(".ref");
    var labels = $(".label");
    labelsMap = {};
    $.each(labels,function(index,label){
        var labelName = label.id;
        labelsMap[labelName] = label;
    });
    var refsWithNumbers = {};
    for (i = 0; i < refs.length; ++i) {
        var ref = refs[i];
        var refname = ref.getAttribute("ref");
        if (labelsMap.hasOwnProperty(refname) ) {
            var label = labelsMap[refname];
            var father = label.parentNode;
            var number = father.getAttribute("counter");
            ref.text = number;
        }


    }



}

function getElementByLabel(labelName) {
    return labelsMap[labelName]
}


