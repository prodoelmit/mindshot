require 'json'
module Sexp


    class Document < Treetop::Runtime::SyntaxNode
        def cl
            return "document"
        end
        def to_html(with_preamble = true, opts)
            s = with_preamble ? %&<!DOCTYPE html>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
            <script type="text/javascript" src="jquery-2.2.1.js"> </script>
            <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=rb"></script>
            <script type="text/javascript" src="jquery.spoiler.min.js"> </script>
            <script async defer src="https://hypothes.is/embed.js"></script>
            <script type="text/javascript" src="main.js"> </script>
            <link rel="stylesheet" href="style.css"/>
            <script src="mathjax_settings.js" type="text/javascript"></script>
            <script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'> </script>
            </head>
            <body>& : ""

            elements.each do |e|
                unless e.nil?
                    if (e.is_a? Content)
                        s += e.paragraph_to_html opts
                    else 
                        s += e.to_html opts
                    end
                end
            end
            s += with_preamble ? "</body></html>" : ""

            return s
        end
    end

    class ContentBlock < Treetop::Runtime::SyntaxNode
        def cl 
            return "ContBlock"
        end
        def to_html(opts)
            #s = " CBLOCKSTART"
            s = ""
            elements.each do |e|
                a = e.to_html opts
                s += a unless a.nil?
            end
            #s += " CBLOCKEND"
            return s
        end
    end

    class Content < Treetop::Runtime::SyntaxNode
        def cl
            return "Content"
        end
        def to_html(opts)
            #s = " CONTENTBEGIN"
            s = ""
            elements.each do |e|
                unless e.nil?
                    s += "#{e.to_html opts}"
                end
            end
            #s += " CONTENTEND"
            return s
        end

        def paragraph_to_html(opts)
            s = "<p>#{self.to_html opts}</p>"
            return s

        end


    end

    class Environment < Treetop::Runtime::SyntaxNode
        def cl
            return "environment"
        end
        def to_html(opts)

            case name
            when "enumerate" 
                return Enumerate::to_html       
            when "spoilerbody"
                s = %$<div class="spoiler-content" data-spoiler-link="#{TexConverter::Counter.pairCounter("spoiler")}">#{content.to_html opts}</div>$
                return s
            else 

                s =  %$<div class="$
                s += name
                s += %$">$
                s += content.to_html opts
                s += %$</div>$
                return s
            end
        end

        def name
            elements[2].text_value
        end

        def content
            elements[5]
        end



    end


    class VerbatimEnv < Treetop::Runtime::SyntaxNode
        def cl
            return "verbatimEnv"
        end
        def to_html(opts)
            #return "verbatimEnv::to_html"
            
            case name
            when "rubycode"

                return %$<pre class="prettyprint">#{content.text_value}</pre>$
            when "juliacode"
                return %$<pre class="prettyprint">#{content.text_value}</pre>$
            when "code"
                return %$<pre class="prettyprint">#{content.text_value}</pre>$
            when "cppcode"
                return %$<pre class="prettyprint">#{content.text_value}</pre>$
            else 
                s =  %$<div class="$
                s += name
                s += " verbatimEnv"
                s += %$">$
                s += content.to_html opts
                s += %$</div>$
                return s
            end
                
        end

        def name
            elements[2].text_value
        end

        def content
            elements[5]
        end



    end


    class Itemize < Treetop::Runtime::SyntaxNode
        def cl
            return "itemize"
        end
        def to_html(opts)
            s = "<ul>"
            items.each do |i|
                s += i.to_html opts
            end
            s += "</ul>"
            return s
        end
        
        def items
            return elements[3].elements
        end
    end

    class ListItem < Treetop::Runtime::SyntaxNode
        def cl
            return "listitem"
        end
        def to_html(opts)
            return %$<li>#{elements[1].to_html opts}</li>$
        end
    end
    class Enumerate < Treetop::Runtime::SyntaxNode
        def cl
            return "enumerate"
        end

        def to_html(opts)
            s = "<ol>"
            items.each do |i|
                s += i.to_html opts
            end
            s += "</ol>"
            return s
        end
        
        def items
            return elements[3].elements
        end
    end


    class Command < Treetop::Runtime::SyntaxNode

        def mayContainInlineFormula(s)
            return s.gsub(/\$([^\$]*)\$/, '\(\1\)')
        end
        def cl
            return "command"
        end
        def to_html(opts)
            case name
            when "section"
                return section_to_html opts
            when "subsection"
                return subsection_to_html opts
            when "subsubsection"
                return subsubsection_to_html opts
            when "paragraph"
                return paragraph_to_html opts
            when "firstdef"
                return firstdef_to_html opts
            when "textit"
                return textit_to_html opts
            when "textbf"
                return textbf_to_html opts
            when "bf"
                return textbf_to_html opts
            when "primech"
                return primech_to_html opts
            when "cImg"
                return cImg_to_html opts
            when "ref"
                return ref_to_html opts
            when "cite"
                return cite_to_html opts
            when "primer"
                return primer_to_html opts
            when "label"
                TexConverter::RefIndex.registerLabel(args[0], TexConverter::Parser.currentPage)
                return %-<span class="label" id="#{args[0]}"></span>-
            when "geogebra"
                return %-<iframe scrolling="no" src="https://www.geogebra.org/material/iframe/id/#{args[0]}/width/1000/height/400/border/888888/rc/false/ai/false/sdz/true/smb/false/stb/false/stbh/true/ld/false/sri/true/at/auto" width="1000px" height="400px" style="border:0px;"> </iframe>-
            when "youtube"
                return %$<iframe width="1000px" height="400px" src="https://www.youtube.com/embed/#{args[0]}" frameborder="0" allowfullscreen></iframe>$
            when "url"
                return %$<a href="#{args[0]}">#{args[1].nil? ? args[0] : args[1]}</a>$
            when "commit"
                if args.size == 1
                    list = args[0].split("/")
                    user = list[3]
                    repo = list[4]
                    commit = list[6]
                    return %$<a href="#{args[0]}">#{repo}/#{commit[0..5]}</a>$
                elsif args.size == 3
                    return %$<a href="http://git.sector32.local/#{args[0]}/#{args[1]}/commit/#{args[2]}">#{args[1]}/#{args[2][0..5]}</a>$
                end
            when "'"
                return %-#{args[0]}\u0301-
            when "linktosection"
                return %-<a class="linktosection" href="<%= TexConverter::RefIndex.getPageByLabel("#{args[0]}") %>">#{args[1]}</a>-
            when "spoilertitle"
                return %$<div class="spoiler" data-spoiler-link="#{TexConverter::Counter.pairCounter("spoiler")}">#{args[0]}</div>$
            when "nextslide"
                return "!@nextslide@!"
            when "newpage"
                return "!@pagebreak@!"
            when "webtitle"
                return %$<div id="webtitle" style="display:none">WEBTITLE::{#{args[0]}}</div>#{section_to_html}$
            when "webauthor"
                return %$<div id="webauthor" style="display:none">WEBAUTHOR::{#{args[0]}}</div>$
            when "weblabelfor" #\weblabel{a0Edit}{\(A_0\)}
                return %$<label for="#{args[0]}">#{mayContainInlineFormula args[1]}</label>$
            when "enabledrag"
                return %$<div id="enabledrag" style="display:none"></div>$
            when "valuewatcher"
                return %$<span class="ggbValueWatcher" id="#{args[0]}">#{args[0]}</span>$
            when "repeatprevpage"
                return %$!@REPEATPREVPAGE@!$
            when "otvet"
                return otvet_to_html opts
            when "resh"
                return resh_to_html opts
            when "webedit"
                id = args[0]
                json = JSON.parse("{#{args[1]}}")
                s = %$<input class="ggbInput" id="#{id}"$
                type = json.delete "type"
                value = json.delete "value"
                onclick = json.delete "onclick"
                if onclick
                    if (type == "checkbox")
                        onclick.gsub!("!@VALUE@!", %$' + this.checked + '$)
                    else
                        onclick.gsub!("!@VALUE@!", %$' + this.value + '$)
                    end
                    onclickLines = onclick.split(";");
                    onclickJS = 'javascript:'
                    onclickLines.each do |l|
                        onclickJS += %$document.ggbApplet.evalCommand('#{l}');$
                    end
                end

                onchange = json.delete "onchange"
                if onchange
                    if (type == "checkbox")
                        onchange.gsub!("!@VALUE@!", %$' + this.checked + '$)
                    else
                        onchange.gsub!("!@VALUE@!", %$' + this.value + '$)
                    end
                    onchangeLines = onchange.split(";");
                    onchangeJS = 'javascript:'
                    onchangeLines.each do |l|
                        onchangeJS += %$document.ggbApplet.evalCommand('#{l}');$
                    end
                end

                onupdate = json.delete "onupdate"
                if onupdate
                    if (type == "checkbox")
                        onupdate.gsub!("!@VALUE@!", %$' + this.checked + '$)
                    else
                        onupdate.gsub!("!@VALUE@!", %$' + this.value + '$)
                    end
                    onupdateLines = onupdate.split(";");
                    onupdateJS = 'javascript:'
                    onupdateLines.each do |l|
                        onupdateJS += %$document.ggbApplet.evalCommand('#{l}');$
                    end
                end

                oninput = json.delete "oninput"
                if oninput
                    if (type == "checkbox")
                        oninput.gsub!("!@VALUE@!", %$' + this.checked + '$)
                    else
                        oninput.gsub!("!@VALUE@!", %$' + this.value + '$)
                    end
                    oninputLines = oninput.split(";");
                    oninputJS = 'javascript:'
                    oninputLines.each do |l|
                        oninputJS += %$document.ggbApplet.evalCommand('#{l}');$
                    end
                end

                s += %$ type="#{type}"$ if type
                s += %$ value="#{value}"$ if value
                s += %$ onclick="#{onclickJS}"$ if onclickJS
                s += %$ onchange="#{onchangeJS}"$ if onchangeJS
                s += %$ onupdate="#{onupdateJS}"$ if onupdateJS
                s += %$ oninput="#{oninputJS}"$ if oninputJS

                json.each do |k, v|
                    key = k.gsub(/\"/,'')
                    s += %$ #{key}=#{v}$
                end

                s += "></input>"


                return s
            else
                if (name == "section") 
                    return section_to_html opts
                end
                s = "<span>"
                s += elements[2].text_value
                args.each do |a|
                    s += " #{a} :::: #{self.name}"
                end
                s += "</span>"
                return s
            end
        end

        def args
            return elements[3].elements.map {|e| e.elements[1].text_value}
        end
        
        def argsR
            return elements[3].elements.map {|e| e.elements[1]} 
        end

        def name
            return elements[2].text_value
        end

        def value
            return elements[3].text_value
        end

        def section_to_html(opts)
            s = "</p><h1>#{mayContainInlineFormula(args[0])}</h1><p>"
            s = s.gsub('\%', "%")
            s = s.gsub("\\\\","<br>")
            s = s.gsub("---","&mdash;")
            s = s.gsub("``","&ldquo;")
            s = s.gsub("''","&rdquo;")
            return s
        end

        def subsection_to_html(opts)
            s = "</p><h2>#{mayContainInlineFormula(args[0])}</h2><p>"
            s = s.gsub('\%', "%")
            s = s.gsub("\\\\","<br>")
            s = s.gsub("---","&mdash;")
            s = s.gsub("``","&ldquo;")
            s = s.gsub("''","&rdquo;")
            return s
        end

        def subsubsection_to_html(opts)
            s = "</p><h3>#{mayContainInlineFormula(args[0])}</h3><p>"
            s = s.gsub('\%', "%")
            s = s.gsub("\\\\","<br>")
            s = s.gsub("---","&mdash;")
            s = s.gsub("``","&ldquo;")
            s = s.gsub("''","&rdquo;")
            return s
        end
        def paragraph_to_html(opts)
            s = "</p><p><b>#{mayContainInlineFormula(args[0])}</b>"
            return s
        end
        def otvet_to_html(opts)
            s = "</p><p><b>Ответ: </b>"
            return s
        end

        def resh_to_html(opts)
            s = "</p><h2>Решение</h2><p>"
            return s
        end

        def firstdef_to_html(opts)
            s = %-<span class="firstdef">#{args[0]}</span>-
            return s
        end

        def textit_to_html(opts)
            s = %-<i>#{args[0]}</i>-
            return s
        end

        def textbf_to_html(opts)
            s = %-<b>#{args[0]}</b>-
            return s
        end

        def primech_to_html(opts)
            s = "<b>Примечание:</b>"
            return s
        end

        def cImg_to_html(opts)
            counter = TexConverter::Counter.getCounter("img")
            s = %&<div class="cImg" counter="#{counter}"><span class="label" id="#{args[3]}"></span><img cwidth="#{args[0]}" src="#{opts[:relative_path]}/figures/#{args[1]}.png" /><div class="caption">Рисунок #{counter} \&mdash; #{argsR[2].to_html opts}</div></div>&

        end

        def ref_to_html(opts)
            s = %&<a class="ref" href="##{args[0]}" ref="#{args[0]}">?</a>&
        end

        def cite_to_html(opts)
            s = %&[<a class="cite" data-href-dummy="/projects/:project_id/publication/#{args[0]}" href="#" cite="#{args[0]}">#{args[0]}</a>]&
        end

        def primer_to_html(opts)
            s = %&<span class="primer">Пример: </span>&

        end
        

    end
    

    class Text < Treetop::Runtime::SyntaxNode
        def cl
            return "text"
        end
        def to_html(opts)
            s = ""
            elements.each do |e|
                unless e.nil?
                        s += e.to_html opts
                end
            end
            #s = " TEXTBEGIN#{text_value}TEXTEND"
            #s = text_value.gsub("\\\\","<br>")
            #s = s.gsub('\%', "%")
            #s = s.gsub("---","&mdash;")
            #s = s.gsub("``","&ldquo;")
            #s = s.gsub("''","&rdquo;")
            return s
        end

    end
    
    class LineBreaks < Treetop::Runtime::SyntaxNode
        def cl
            return "linebreaks"
        end
        def to_html(opts)
            return "</p> <p>"
        end

    end

    class Word < Treetop::Runtime::SyntaxNode
        def cl 
            return "word"
        end
        def to_html(opts)
            s = text_value.gsub("\\\\", "<br>")
            s = s.gsub('\%', "%")
            s = s.gsub('\\`', "`")
            s = s.gsub("---","&mdash;")
            s = s.gsub("``","&ldquo;")
            s = s.gsub("''","&rdquo;")
            return s
        end
    end

    class LineBreak < Treetop::Runtime::SyntaxNode
        def cl
            return "linebreak"
        end
        def to_html(opts)
            #return "BR"
        end
    end

    class SpaceLiteral < Treetop::Runtime::SyntaxNode
        def cl
            return "spaceliteral"
        end
        def to_html(opts)
            return text_value
            #return "SP"
        end
    end


    class InlineFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "inlform"
        end
        def to_html(opts)
            content = elements[1].text_value
            content = content.gsub(/\\vv/,"\\vec")

            s = '\('
            #WARNING: shouldn't this use content instead of elements[1].text_value? 
            s += elements[1].text_value
            s += '\)'
            return s
        end

    end

    class InlineCode < Treetop::Runtime::SyntaxNode
        def cl
            return "inlcode"
        end
        def to_html(opts)
            Rails.logger.debug "Met inlineCode : " + elements[1].text_value
            content = elements[1].text_value
            s = '<code>'
            s += content.gsub(/\\`/,"`").gsub(/\\\\/,"\\")
            s += '</code>'
        end
    end


    class DollarFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "dolform"
        end
        def to_html(opts)
            #puts "compiling equation: \n #{elements[1]}"
            s = %-<div class="equationstar">-
            s += %-<img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/>-
            s += %-</div>-
            return s
        end

    end


    class BracketFormula < Treetop::Runtime::SyntaxNode
        def cl
            return "bracform"
        end
        def to_html(opts)
            s = %-<div class="equationstar">-
            s += %-<img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/>-
            s += %-</div>-
            return s
        end

    end

    class Equation < Treetop::Runtime::SyntaxNode
        def cl
            return "eq"
        end
        def to_html(opts)
            counter = TexConverter::Counter.getCounter("equation")
            re = Regexp.new "\\label{(?<label>[a-zA-Z0-9:]+)}"
            match = text_value.match re
            unless (match.nil?) 
                label = match[1]
            end


            
            s = %-<div class="equation" counter="#{counter}">-
            s += %-<span class="label" id="#{label}"></span>- unless label.nil?
            s += %-<div class="eqcounter">(#{counter})</div><img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/></div>-
            return s
        end

    end

    class EquationStar < Treetop::Runtime::SyntaxNode
        def cl
            return "eqstar"
        end
        def to_html(opts)
            s = %-<div class="equationstar"><img src="#{TexConverter::TexCompiler.compile_equation(elements[1].text_value)}"/></div>-
            return s
        end

    end

    class Align < Treetop::Runtime::SyntaxNode
        def cl
            return "align"
        end
        def to_html(opts)
            counter = TexConverter::Counter.getCounter("equation")
            re = Regexp.new "\\label{(?<label>[a-zA-Z0-9:]+)}"
            match = text_value.match re
            unless (match.nil?) 
                label = match[1]
            end

            s = %-<div class="align" counter="#{counter}">-
            s += %-<span class="label" id="#{label}"></span>- unless label.nil?
            s += %-<div class="eqcounter">(#{counter})</div><img src="#{TexConverter::TexCompiler.compile_align(elements[1].text_value)}"/></div>-


            return s
        end

    end

    class AlignStar < Treetop::Runtime::SyntaxNode
        def cl
            return "alignstar"
        end
        def to_html(opts)
            s = %-<div class="alignstar"><img src="#{TexConverter::TexCompiler.compile_align(elements[1].text_value)}"/></div>-

            return s
        end

    end

    class Gather < Treetop::Runtime::SyntaxNode
        def cl
            return "gather"
        end
        def to_html(opts)
            counter = TexConverter::Counter.getCounter("equation")
            re = Regexp.new "\\label{(?<label>[a-zA-Z0-9:]+)}"
            match = text_value.match re
            unless (match.nil?)
                label = match[1]
            end

            s = %-<div class="gather" counter="#{counter}">-
            s += %-<span class="label" id="#{label}"></span>- unless label.nil?
            s += %-<div class="eqcounter">(#{counter})</div><img src="#{TexConverter::TexCompiler.compile_gather(elements[1].text_value)}"/></div>-


            return s
        end

    end

    class GatherStar < Treetop::Runtime::SyntaxNode
        def cl
            return "gatherstar"
        end
        def to_html(opts)
            s = %-<div class="gatherstar"><img src="#{TexConverter::TexCompiler.compile_gather(elements[1].text_value)}"/></div>-

            return s
        end

    end

    class FormulaText < Treetop::Runtime::SyntaxNode

    end
    class FormulaEnvironment < Treetop::Runtime::SyntaxNode

    end

    
    class CommandArgContent < Treetop::Runtime::SyntaxNode


    end



end

class Label
    def initialize(name)
        @@name = name
    end

    def to_html(opts)
        %-<span id="#{@@name}"></span>-
    end


end

module Treetop
    module Runtime
        class SyntaxNode
            def inspect 
                return text_value
            end
            def to_html(opts)
                if ((not nil?) and
                    (not elements.nil?) and
                    elements.length > 0)
                    s = ""
                    elements.each do |e|
                        unless e.nil?
                            a = e.to_html opts
                            s += a unless a.nil?
                        end
                    end
                    return s
                else 
                    return ""
                end
            end
        end
    end
end
